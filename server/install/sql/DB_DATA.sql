-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 09-12-2019 a las 08:18:40
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `car_accident_database`
--

--
-- Volcado de datos para la tabla `POLICIES`
--

INSERT INTO `POLICIES` (`ID`, `ID_USER`, `COMPANY`, `CAR_MODEL`, `CAR_AGE`, `CAR_PLATE_NUMBER`, `POLICY_NUMBER`) VALUES
(1, 1, 'Compañia de Seguros 1', 'Toyota Auris', 7, '2233-JKH', '11111111111111'),
(2, 3, 'Compañia de Seguros 2', 'Audi A4', 5, '4578HPJ', '212121212222');

--
-- Volcado de datos para la tabla `USERS`
--

INSERT INTO `USERS` (`ID`, `LOGIN`, `PASSWORD`, `NAME`, `ROLE`, `DRIVER_LICENSE_NUMBER`, `NIF`, `EMAIL`, `PHONE`) VALUES
(0, 'vicente', 'vicente', 'Vicente Simorte Bardají', 1, '254343563', '987654321', 'mail@pruebas.es', '97687365252'),
(1, 'luis', 'luis', 'Luis Alberto Gómez Peréz', 0, '173643546', '2222222222', 'mail2@pruebas.es', '9876542323'),
(2, 'juan', 'juan', 'Juan Antonio Abadias', 5, '', '5555555555', 'mail2@pruebas.es', '6542342344'),
(3, 'pedro', 'pedro', 'Pedro Alfonso Maestro', 2, '1635344666', '9876546776', 'mail6@pruebas.es', '765243776');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
