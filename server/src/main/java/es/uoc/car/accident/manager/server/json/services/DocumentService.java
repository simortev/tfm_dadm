package es.uoc.car.accident.manager.server.json.services;

import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import es.uoc.car.accident.manager.server.jpa.beans.DocumentBean;
import es.uoc.car.accident.manager.server.jpa.daos.AgendaDao;
import es.uoc.car.accident.manager.server.jpa.daos.DocumentDao;
import es.uoc.car.accident.manager.server.log.ServiceLogger;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.List;

/**
 * This class provide document service , to upload or update a file document a multi mime path is sent
 */

@Path("/document")
public class DocumentService extends ServiceLogger {

    @POST
    @Path("/element")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addDocument(
        @FormDataParam("file") InputStream documentStream,
        @FormDataParam("info") String documentJson ) {
        try {
            return Response.ok().entity( DocumentDao.addDocument( documentJson , documentStream ) ).build();
        }
        catch ( Throwable e ){
           m_log.error( "ERROR " , e);
            return Response.status( RestServiceException.getStatus( e ) ).build();
        }
    }

    /**
     * Update the document bean , if user has permission on the  event
     * @param documentBean the doucument bean to be updated
     * @return the response with the agenda bean updated
     */
    @PUT
    @Path("/element")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateAgendaEvent( DocumentBean documentBean ) {

        try {
            return Response.ok().entity( DocumentDao.updateDocument( documentBean ) ).build();
        }
        catch ( Throwable e ){
            m_log.error( "ERROR" , e);
            return Response.status( RestServiceException.getStatus( e )).entity( documentBean ).build();
        }
    }
    /**
     * Get documents of this agenda event
     * @param agenda_id the id of the agenda event to get all the documents
     * @return the list of documents or empty list
     */
    @GET
    @Path("/agenda/{agenda_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDocumentsFromAgendaId(@PathParam("agenda_id") Long agenda_id) {
        try {
            GenericEntity entity =
                new GenericEntity<List<DocumentBean>>( DocumentDao.getDocuments( agenda_id) ) {};
            return Response.ok().entity( entity ).build();
        }
        catch ( RestServiceException e ){
            e.printStackTrace();
            return Response.status( e.getStatus() ).build();
        }
    }

    /**
     * Get documents of this agenda event
     * @param documentId the id of the document
     * @return the list of documents or empty list
     */
    @GET
    @Path("/element/{document_id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getDocumentsFromId(@PathParam("document_id") Long documentId) {
       return DocumentDao.getDocument( documentId );
    }

    /**
     * Remove an event ( if user has permission )
     * @param documentId the event id
     * @return the event removed
     */
    @DELETE
    @Path("/element/{document_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteDocumentEvent(@PathParam("document_id") Long documentId) {
        try {
            return Response.ok().entity( DocumentDao.removeDocument( documentId ) ).build();
        }
        catch ( Throwable e ){
            m_log.error( "ERROR" , e);
            return Response.status( RestServiceException.getStatus( e ) ).build();
        }
    }

}
