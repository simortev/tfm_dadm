package es.uoc.car.accident.manager.server.log;

import es.uoc.car.accident.manager.server.jpa.beans.interfaces.IObjectBean;
import es.uoc.car.accident.manager.server.thread.local.manager.ThreadLocalManager;
import org.slf4j.LoggerFactory;

public class ServiceLogger {
    /** The logger */
    protected org.slf4j.Logger m_log;

    /**
     * Bean operation
     */
    public enum BeanOperation{
        ADD,
        UPDATE,
        REMOVE
    }

    /** Constructor */
    public ServiceLogger() {
        m_log = LoggerFactory.getLogger(getClass());
    }

    /**
     * Get user name
     * @return the user name
     */
    private static String getUserName() {
        return ThreadLocalManager.getUserBean().getName();
    }

    /**
     * Trace information
     * @param objectBean the object added, removed or updated
     * @param operation the operation added, removed or updated
     * @param cl
     */
    public static void info(Class<?> cl, BeanOperation operation  , IObjectBean objectBean ){
        LoggerFactory.getLogger(cl).info( "{} {} {}:\n{}" ,
            getUserName(),
            operation.name().toUpperCase() ,
            objectBean.getClass().getSimpleName().toUpperCase(),
            objectBean );
    }

    /**
     * Trace info message
     * @param cl
     * @param message
     */
    public static void info(Class<?> cl, String message ){
        LoggerFactory.getLogger(cl).info(  "{} {}" ,  getUserName() , message );
    }

    /**
     * Trace information
     * @param objectBean the object added, removed or updated
     * @param operation the operation added, removed or updated
     * @param cl
     * @param message the error message
     */
    public static void error(Class<?> cl, BeanOperation operation  , IObjectBean objectBean , String message ){
        LoggerFactory.getLogger(cl).warn( "{} {} {}: {}\n{}" ,
            getUserName(),
            operation.name().toUpperCase() ,
            objectBean.getClass().getSimpleName().toUpperCase(),
            message,
            objectBean );
    }



    /**
     * Trace error
     * @param objectBean the object
     * @param operation the operation add, update, remove
     * @param cl the class
     * @param error the error
     */
    public static void error(
        Class<?> cl,
        BeanOperation operation ,
        IObjectBean objectBean ,
        Throwable error ){
        LoggerFactory.getLogger(cl).error(
            getUserName() + " " +
            operation.name().toUpperCase() + " " +
            objectBean.getClass().getSimpleName().toUpperCase() + ":\n" +
            objectBean , error );
    }
}
