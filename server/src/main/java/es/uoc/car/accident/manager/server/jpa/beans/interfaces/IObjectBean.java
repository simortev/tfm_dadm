package es.uoc.car.accident.manager.server.jpa.beans.interfaces;

import javax.xml.bind.annotation.XmlTransient;


public interface IObjectBean  {
    /**
     * Get id
     * @retur the object id
     */
    @XmlTransient
    Long getId();
}
