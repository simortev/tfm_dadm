package es.uoc.car.accident.manager.server.jpa.beans;

import es.uoc.car.accident.manager.server.jpa.beans.interfaces.IObjectBean;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Optional;

@Entity
@Cacheable(false)
@Table(name = "USERS")
public class UserBean implements IObjectBean,Serializable {

    @Id
    @Column(name="ID")
    @XmlElement(name="id")
    private Long m_lID;

    @Column(name="LOGIN")
    @XmlElement(name="login")
    private String m_sLogin;

    @Column(name="PASSWORD")
    @XmlTransient
    private String m_sPassword;

    @Column(name="NAME")
    @XmlElement(name="name")
    private String m_sName;

    @Column(name="ROLE")
    @XmlElement(name="role")
    private Integer m_iRole;

    /** The driver license number */
    @Column(name="DRIVER_LICENSE_NUMBER")
    @XmlElement(name="driver_license_number")
    private String m_sDriverLicenseNumber;

    /** The user nif */
    @Column(name="NIF")
    @XmlElement(name="nif")
    private String m_sNif;

    /** The user mail */
    @Column(name="EMAIL")
    @XmlElement(name="mail")
    private String m_sMail;

    /** The user phone */
    @Column(name="PHONE")
    @XmlElement(name="phone")
    private String m_sPhone;

    /**
     * The enum with roles
     */
    public enum Role {
        CUSTOMER(0),
        INSURANCE_STAFF(1),
        CAR_DRIVER(2),
        TOW_TRUCK_DRIVER(3),
        LOSS_ADJUSTER(4),
        CAR_MECHANIC(5),
        LAWYER(6),
        MEDICAL_PRACTITIONER(7),
        PHYSIOTHERAPIST(8);

        /** The role id */
        private int m_iId;

        /**
         * Constructor
         * @param iId the role id
         */
        Role(int iId) {
            m_iId = iId;
        }

        /**
         * Get role id
         * @return the role id
         */
        public int id() {
            return m_iId;
        }
    }

    public UserBean() {

    }

    @XmlTransient
    @Override
    public Long getId() {
        return m_lID;
    }

    @XmlTransient
    public String getLogin() {
        return m_sLogin;
    }

    @XmlTransient
    public String getPassword() {
        return m_sPassword;
    }

    @XmlTransient
    public String getName() {
        return m_sName;
    }

    @XmlTransient
    public int getRole() {
        return m_iRole;
    }

    @XmlTransient
    public String getDriverLicenseNumber(){
        return m_sDriverLicenseNumber;
    }

    @XmlTransient
    public String getNif() {
        return m_sNif;
    }

    @XmlTransient
    public String getMail() {
        return m_sMail;
    }

    @XmlTransient
    public String getPhone() {
        return m_sPhone;
    }

    public void setID(Long m_lID) {
        this.m_lID = m_lID;
    }

    public void setLogin(String m_sLogin) {
        this.m_sLogin = m_sLogin;
    }

    public void setPassword(String m_sPassword) {
        this.m_sPassword = m_sPassword;
    }

    public void setName(String m_sName) {
        this.m_sName = m_sName;
    }

    public void setRole(int iRole ) {
        m_iRole = iRole;
    }

    public void setDriverLicenseNumber( String sDriverLicenseNumber ) {
        m_sDriverLicenseNumber = sDriverLicenseNumber;
    }

    public void setNif( String sNif ){
        m_sNif = sNif;
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();

        Optional
                .ofNullable( m_lID )
                .ifPresent( id -> builder.append("ID: ").append( id ).append( "\n") );
        Optional
                .ofNullable( m_sLogin )
                .ifPresent( login ->
                        builder.append("LOGIN: ").append( m_sLogin ).append( "\n") );

        Optional
                .ofNullable( m_sName )
                .ifPresent( description ->
                        builder.append("NAME: ").append( m_sName ).append( "\n") );

        Optional
            .ofNullable( m_sNif)
            .ifPresent( description ->
                builder.append("NIF: ").append( m_sNif ).append( "\n") );

        Optional
            .ofNullable( m_iRole )
            .ifPresent( role ->
                builder.append("ROLE: ").append( m_iRole ).append( "\n") );

        Optional
            .ofNullable( m_sDriverLicenseNumber )
            .ifPresent( driverLicenseNumber ->
                builder.append("DRIVER LICENSE: ").append( m_sDriverLicenseNumber ).append( "\n") );

        Optional
            .ofNullable( m_sMail)
            .ifPresent( mail ->
                builder.append("MAIL: ").append( mail ).append( "\n") );

        Optional
            .ofNullable( m_sPhone)
            .ifPresent( phone ->
                builder.append("PHONE: ").append( phone ).append( "\n") );

        return builder.toString();
    }
}
