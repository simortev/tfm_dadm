package es.uoc.car.accident.manager.server.jpa.daos;

import es.uoc.car.accident.manager.server.jpa.beans.PolicyBean;
import es.uoc.car.accident.manager.server.jpa.beans.UserBean;
import es.uoc.car.accident.manager.server.jpa.beans.extended.CarInfoBean;
import es.uoc.car.accident.manager.server.jpa.beans.interfaces.IObjectBean;
import es.uoc.car.accident.manager.server.thread.local.manager.ThreadLocalManager;

import javax.persistence.Query;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ObjectDao {

    /** To avoid this class can be instantiated */
    private ObjectDao() {

    }

    /**
     * Get a map , object id and object matching the query, in a in SQL sentence
     * @param sTable The query on table
     * @param sCondition the condition on table list
     * @param sParameter the parameter name
     * @param hIds the parameter lsit of value
     * @param <T> the type of object list return
     * @return a list of objects matching the condition
     */
    public static<T extends IObjectBean,K> Map<Long,T> getObjectMap(
        String sTable,
        String sCondition,
        String sParameter,
        List<K> hIds){

        if( hIds == null || hIds.isEmpty() ) {
            return new HashMap<>();
        }

        List<T> hObjects = getObjectList( sTable, sCondition , sParameter, hIds );

        return Optional
            .ofNullable( hObjects )
            .orElseGet(ArrayList::new)
            .stream()
            .sequential()
            .collect(Collectors.toMap( IObjectBean::getId ,  Function.identity() ) );
    }

    /**
     *
     * @param sTable The query on table
     * @param sCondition the condition on table list
     * @param sParameterName the parameter name
     * @param parameter the parameter value
     * @param <T> the type of object list return
     * @return a list of objects matching the condition
     */
    public static<T> List<T> getObjectList(
        String sTable,
        String sCondition,
        String sParameterName,
        Object parameter ) {

        StringBuilder query = new StringBuilder();
        query.append( sTable );
        query.append( sCondition );

        return Optional
            .ofNullable( query )
            .map( StringBuilder::toString)
            .map( ThreadLocalManager.getRecursiveTransaction()::createQuery )
            .map( q ->  q.setParameter( sParameterName, parameter ) )
            .<List<T>>map( Query::getResultList)
            .orElseGet(ArrayList::new)
            .stream()
            .sequential()
            .collect(Collectors.toList());
    }


    /**
     * Get a list of car info beans ( car accident or damages ) including policy and user info
     * @param hBeans the object attached to policy and user
     * @param constructor the constructor of CarInfoBean
     * @param getPolicyId function to ge policy id of bean
     * @return a list of car info beans of type T ( car accidents or damages )
     */
    public static<T extends CarInfoBean, K > List<T> getCarInfoBeans(
        List<K> hBeans,
        BiFunction<K,CarInfoBean,T> constructor,
        Function<K,Long> getPolicyId)
    {
        if( hBeans == null || hBeans.isEmpty() ){
            return new ArrayList<>();
        }

        List<Long> hPolicyIds = new ArrayList<>();
        Optional
            .ofNullable(hBeans )
            .orElseGet(ArrayList::new)
            .stream()
            .sequential()
            .map(getPolicyId::apply)
            .filter(Objects::nonNull)
            .filter( policyId -> hPolicyIds.contains( policyId ) == false )
            .forEach( hPolicyIds::add );

        Map<Long,PolicyBean> hPolicies = PolicyDao.getPolicies(hPolicyIds);

        List<Long> hUserId = new ArrayList<>();
        Optional
            .ofNullable(hPolicies )
            .map(Map::values)
            .orElseGet(HashSet::new)
            .stream()
            .sequential()
            .map(PolicyBean::getUserId )
            .filter(Objects::nonNull)
            .filter( userId -> hUserId.contains( userId ) == false )
            .forEach( hUserId::add );

        Map<Long,UserBean> hUsers = UserDao.getUsers(hUserId);

        List<T> hCarInfoBeans = new ArrayList<>();
        Optional
            .ofNullable(hBeans)
            .orElseGet(ArrayList::new)
            .stream()
            .sequential()
            .map( bean -> createCarInfoBean( constructor , getPolicyId, bean , hPolicies , hUsers) )
            .filter(Objects::nonNull)
            .forEach( hCarInfoBeans::add );

        return hCarInfoBeans;
    }

    /**
     * Create car info bean ( car accidents and damages include the user and policy
     * @param constructor the constructor of CarInfoBean
     * @param getPolicyId function to ge policy id of bean
     * @param hPolicies the policies
     * @param hUsers the users
     * @return the car accidents
     */
    protected static<T extends CarInfoBean,K> T createCarInfoBean(
        BiFunction<K,CarInfoBean,T> constructor,
        Function<K,Long> getPolicyId,
        K bean ,
        Map<Long,PolicyBean> hPolicies,
        Map<Long,UserBean> hUsers) {

        PolicyBean policy = Optional
            .ofNullable(bean)
            .map(getPolicyId::apply)
            .map(hPolicies::get)
            .orElse(null);

        UserBean user = Optional
            .ofNullable(policy)
            .map(PolicyBean::getUserId)
            .map(hUsers::get)
            .orElse(null);

        if( policy == null || user == null ){
            return null;
        }

        return constructor.apply( bean , new CarInfoBean( policy , user ) );
    }

}
