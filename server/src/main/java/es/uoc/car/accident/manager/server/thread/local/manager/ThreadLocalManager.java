package es.uoc.car.accident.manager.server.thread.local.manager;

import es.uoc.car.accident.manager.server.jpa.beans.UserBean;
import es.uoc.car.accident.manager.server.json.services.RecursiveTransaction;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Optional;

/**
 * This class stores a entity manager for JPA functionality for each thread
 *
 * When from a servlet or a json service the thread entity manager is requested
 *
 * In this way only a entity manager is created for a thread and only one
 * is share in all java object
 *
 * Every entity manager belongs to a thread
 *
 */
public class ThreadLocalManager {

    /** The persistence unit name */
    private static final String PERSISTENCE_UNIT_NAME = "car_accident_manager";
    /**
     * Thread entity constructor is privated to avoid to be instantiated
     * All the methods are private
     */
    private ThreadLocalManager() {

    }

    /** Thread local to store entities managers, it is a singleton */
    private static final ThreadLocal<RecursiveTransaction> m_RecursiveTransactionThreadLocal = new ThreadLocal<>();

    /** Thread local to store the user bean, it is a singleton */
    private static final ThreadLocal<UserBean> m_userBeanThreadLocal = new ThreadLocal<>();

    /**
     * Get entity manager from thread local or create if
     * @return the entity manager
     */
    public  static RecursiveTransaction getRecursiveTransaction() {

        return Optional
            .ofNullable(m_RecursiveTransactionThreadLocal)
            .map( ThreadLocal::get )
            .orElseGet( ThreadLocalManager::createRecursiveTransaction );
    }

    /**
     * Get entity manager from thread local or create if
     * @return the entity manager
     */
    public  static UserBean getUserBean() {

        return Optional
            .ofNullable(m_userBeanThreadLocal)
            .map( ThreadLocal::get )
            .orElse( null );
    }


    /**
     * Create entity manager and include it in thread local
     * @return the entity manager
     */
    protected static RecursiveTransaction createRecursiveTransaction() {

        RecursiveTransaction recursiveTransaction = Optional
            .ofNullable( PERSISTENCE_UNIT_NAME )
            .map( Persistence::createEntityManagerFactory )
            .map( EntityManagerFactory::createEntityManager )
            .map(RecursiveTransaction::new)
            .orElse( null );

        Optional
            .ofNullable( recursiveTransaction )
            .ifPresent( m_RecursiveTransactionThreadLocal::set );

        return recursiveTransaction;
    }

    /**
     * Add a user bean include in the thread local
     */
    public static void addUserBeanInThreadLocal( UserBean userBean ) {

        Optional
            .ofNullable( userBean )
            .ifPresent( m_userBeanThreadLocal::set );
    }

}
