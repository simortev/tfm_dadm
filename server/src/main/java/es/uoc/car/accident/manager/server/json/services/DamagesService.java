package es.uoc.car.accident.manager.server.json.services;

import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import es.uoc.car.accident.manager.server.jpa.beans.DamageBean;
import es.uoc.car.accident.manager.server.jpa.beans.extended.CarDamageBean;
import es.uoc.car.accident.manager.server.jpa.daos.AgendaDao;
import es.uoc.car.accident.manager.server.jpa.daos.DamageDao;
import es.uoc.car.accident.manager.server.log.ServiceLogger;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * This class provides all the RestFul services for damages data
 *
 * Get damages of a car accident
 * Add, update or delete a damage
 *
 */

@Path("/damages")
public class DamagesService extends ServiceLogger {

    /**
     * Get only an agenda event by its id ( if user has permission )
     * @param id the id of the agenda event
     * @return the event or null
     */
    @GET
    @Path("/element/{damages_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDamagesById(@PathParam("damages_id") Long id) {

        DamageBean damageBean = DamageDao.getDamageById( id );
        if( damageBean != null ) {
            GenericEntity entity = new GenericEntity<DamageBean>(damageBean) {};
            return Response.ok(entity).build();
        }
        else{
            return Response.status(Response.Status.NOT_FOUND ).build();
        }
    }


    /**
     * Get the events of a parent  ( if user has permission )
     * @param lCarAccidentId the parent id
     * @return a list of child events
     */
    @GET
    @Path("/agenda/{agenda_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDamagesOfCarAccident(@PathParam("agenda_id") Long lCarAccidentId ) {
        try {
            GenericEntity entity = new GenericEntity<List<CarDamageBean>>(DamageDao.getDamages(lCarAccidentId)) {};
            return Response.ok().entity( entity ).build();
        }
        catch ( RestServiceException e ){
            m_log.error( "ERROR: {}" , e.getMessage() );
            return Response.status( e.getStatus() ).build();
        }
    }

    /**
     * Add an damage bean , if user has permission
     * @param damageBean the damage bean to be added
     * @return the damage bean response
     */
    @POST
    @Path("/element")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addDamages( DamageBean damageBean ) {

        try {
            return Response.ok().entity( DamageDao.addDamage( damageBean ) ).build();
        }
        catch ( Throwable e ){
            m_log.error( "ERROR: " , e );
            return Response.status( RestServiceException.getStatus( e )).entity( damageBean ).build();
        }
    }


    /**
     * Add an damage bean , if user has permission
     * @param damageBean the damage bean to be added
     * @return the damage bean response
     */
    @POST
    @Path("/policy/{policy_number}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addDamages( DamageBean damageBean , @PathParam("policy_number") String sPolicyNumber  ) {

        try {
            return Response.ok().entity( DamageDao.addDamage( damageBean , sPolicyNumber) ).build();
        }
        catch ( Throwable e ){
            m_log.error( "ERROR: " , e );
            return Response.status( RestServiceException.getStatus( e )).entity( damageBean ).build();
        }
    }

    /**
     * Update an damage bean , if user has permission
     * @param damageBean the damage bean to be updated
     * @return the damage bean response
     */
    @PUT
    @Path("/element")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateDamage( DamageBean damageBean ) {

        try {
            return Response.ok().entity( DamageDao.updateDamageBean( damageBean ) ).build();
        }
        catch ( Throwable e ){
            m_log.error( "ERROR:" , e );
            return Response.status( RestServiceException.getStatus( e )).entity( damageBean ).build();
        }
    }

    /**
     * Remove an damage ( if user has permission )
     * @param damageId the damage id
     * @return the damage removed
     */
    @DELETE
    @Path("/element/{damage_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteDamage(@PathParam("damage_id") Long damageId ) {
        try {
            return Response.ok().entity( DamageDao.removeDamageBean( damageId ) ).build();
        }
        catch ( Throwable e ){
            m_log.error( "ERROR: {}" , e.getMessage() );
            return Response.status( RestServiceException.getStatus( e ) ).build();
        }
    }
}
