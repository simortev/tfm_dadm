package es.uoc.car.accident.manager.server.filters;

import es.uoc.car.accident.manager.server.jpa.beans.UserBean;
import es.uoc.car.accident.manager.server.thread.local.manager.ThreadLocalManager;
import es.uoc.car.accident.manager.server.jpa.daos.DocumentDao;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;

import static es.uoc.car.accident.manager.server.constants.CarAccidentServiceConstants.DOCUMENT_DIRECTORY;
import static es.uoc.car.accident.manager.server.constants.CarAccidentServiceConstants.USER_SESSION_ATTRIBUTE;

/**
 * This filter is responsible to know if a user is in session
 * and if not return an error
 *
 * In other case inject user session bean using thread local mmanager
 * in order to be got by the JSON services and to limit the request
 * of information only to the data the user has been granted
 */
public class AuthenticationFilter implements Filter {

    /** User name label */
    private final static String USER_NAME_LABEL = "USER";

    /** The service name */
    private final static String SERVICE_NAME = "SERVICE";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

        /** Set directory for documents */
        Optional
            .ofNullable( filterConfig )
            .map( FilterConfig::getServletContext )
            .map( servletContext -> servletContext.getInitParameter( DOCUMENT_DIRECTORY ) )
            .ifPresent( DocumentDao::setDocumentDirectory );

        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");

        MDC.put( USER_NAME_LABEL , SERVICE_NAME );
        LoggerFactory.getLogger( AuthenticationFilter.class ).info( "Starting Car Service {}" , new Date() );
        MDC.remove( USER_NAME_LABEL );
    }

    @Override
    public void doFilter( ServletRequest request,
                          ServletResponse response,
                          FilterChain chain ) throws IOException, ServletException {

        /** If user is in session inject user bean in thread local manager */
        UserBean userBean = Optional
            .ofNullable( request )
            .map( HttpServletRequest.class::cast )
            .map( servletRequest -> servletRequest.getSession( false ) )
            .map( session -> session.getAttribute( USER_SESSION_ATTRIBUTE ) )
            .map( UserBean.class::cast )
            .orElse( null );

        Optional
            .ofNullable( userBean )
            .ifPresent( ThreadLocalManager::addUserBeanInThreadLocal );

        if( userBean == null )
        {
            MDC.put( USER_NAME_LABEL , SERVICE_NAME );

            LoggerFactory
                .getLogger( AuthenticationFilter.class )
                .error( "User is not in session {}"  ,
                    ( (HttpServletRequest) request ).getRequestURL().toString() );

            MDC.remove( USER_NAME_LABEL );
        }

        /** If user is not in session then exit*/
        Optional
            .ofNullable( userBean )
            .orElseThrow( () -> new ServletException( "User is not in session" ) );

        String sUserName = Optional
            .ofNullable( userBean )
            .map( UserBean::getName )
            .orElse( SERVICE_NAME );

        MDC.put( USER_NAME_LABEL , sUserName );

        chain.doFilter( request , response );

        // Clear entity manager
        ThreadLocalManager.getRecursiveTransaction().clear();

        MDC.remove( USER_NAME_LABEL );
    }

    @Override
    public void destroy() {

    }
}
