package es.uoc.car.accident.manager.server.jpa.daos;

import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean.AgendaItemType;
import es.uoc.car.accident.manager.server.jpa.beans.extended.CarAccidentBean;
import es.uoc.car.accident.manager.server.jpa.beans.UserBean;
import es.uoc.car.accident.manager.server.json.services.RecursiveTransaction;
import es.uoc.car.accident.manager.server.json.services.RestServiceException;
import es.uoc.car.accident.manager.server.log.ServiceLogger;
import es.uoc.car.accident.manager.server.thread.local.manager.ThreadLocalManager;
import org.slf4j.LoggerFactory;


import javax.persistence.Query;
import java.time.*;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

import static es.uoc.car.accident.manager.server.constants.CarAccidentServiceConstants.*;
import static es.uoc.car.accident.manager.server.jpa.beans.AgendaBean.AgendaItemType.CAR_ACCIDENT;
import static es.uoc.car.accident.manager.server.jpa.beans.AgendaBean.AgendaItemType.EVENT;
import static es.uoc.car.accident.manager.server.log.ServiceLogger.BeanOperation.*;
import static java.time.LocalTime.MIDNIGHT;

public class AgendaDao  {

    /** Provide id for agenda events */
    private static AutoIncrementProvider m_autoIncrementProvider = new AutoIncrementProvider();

    /** One day */
    private final static int ONE_DAY = 1;


    /**
     * To avoid this class can be instantiated
     */
    private AgendaDao() {

    }

    /**
     * Get agenda events with aditional condtions,not only grant condition
     * @param conditions the addtional conditions
     * @return the list of event who match the conditions
     */
    protected static List<AgendaBean> getAgendaEvents(Map<String,Object> conditions ){

       UserBean userBean = ThreadLocalManager.getUserBean();

        /** select from agenda, grants where event.id = grant.agenda_id and user.id = session user id*/
        StringBuilder query = new StringBuilder();
        query.append( SELECT_EVENT_FROM_AGENDA_JOIN_GRANT_TABLES );
        query.append( AGENDA_TABLE_USER_PERMISSION_CONDITION );

        /** Complete query with additional conditions table.columns = :XZYTT where
         * the XZYTT is the hash code of the value
         *
         * At the end the query will be
         *
         *  SELECT event FROM AgendaBean as event , GrantBean as grant
         *
         *      where event.m_lID = grant.m_lAgendaId and grant.m_lUserId = :userId  ->  Event grant condition
         *      and table1.columnX = :COLUMN_value1_hash
         *      and table2.columnY = :COLUMN_value2_hash
         *      and table3.columnZ = :COLUMN_value3_hash
         *
         * */
        Optional
            .ofNullable( conditions )
            .orElseGet( HashMap::new )
            .entrySet()
            .stream()
            // No parallel
            .sequential()
            .forEach( parameter ->
                query.append(
                    String.format( ADDITIONAL_CONDITION_FORMAT , parameter.getKey() ,
                        Math.abs( parameter.getValue().hashCode() ) ) ) );

        /**
         * Set parameters, the first one the user id parameter
         * And the other one, they have been referenced as the hash of the object
         */
        return Optional
            .ofNullable( query )
            .map( StringBuilder::toString)
            .map( ThreadLocalManager.getRecursiveTransaction()::createQuery )
            // Set user parameter
            .map( q ->  q.setParameter( SQL_USER_ID_PARAMETER, userBean.getId() ) )
            // Add other conditions
            .map( q -> addAdditionalConditions( q, conditions) )
            // Get result
            .map( Query::getResultList)
            .orElse( null );
    }

    /**
     * Add additional conditions
     * @param query the sql query
     * @param conditions the additional conditions
     * @return the SQl query
     */
    protected static Query addAdditionalConditions(Query query , Map<String,Object> conditions ) {

        Optional
            .ofNullable( conditions )
            .orElseGet( HashMap::new )
            .entrySet()
            .stream()
            // No parallel
            .sequential()
            .forEach( parameter ->
                query.setParameter(
                    // Parameter name COLUMN_hash_code
                    String.format( ADDITIONAL_CONDITION_PARAMETER_NAME_FORMAT, Math.abs( parameter.getValue().hashCode() ) ) ,
                    // Parameter value
                    parameter.getValue() )  );

        return query;
    }

    /**
     * Get events between 2 different dates
     * @param minLocalDate the min date
     * @param maxLocalDate the max date, it can be null
     * @param sParameter the paramter to find
     * @return list of events
     */
    public static List<AgendaBean> getEvents( LocalDateTime minLocalDate, LocalDateTime maxLocalDate, String sParameter ) {

        UserBean userBean = ThreadLocalManager.getUserBean();

       Date minDate = Date.from( minLocalDate.atZone( ZoneId.systemDefault()).toInstant());
       Date maxDate = Optional
           .ofNullable(maxLocalDate)
           .map(date ->  Date.from( date.atZone( ZoneId.systemDefault()).toInstant()) )
           .orElse(null);


        /** select from agenda, grants where event.id = grant.agenda_id and user.id = session user id*/
        StringBuilder query = new StringBuilder();
        query.append( SELECT_EVENT_FROM_AGENDA_JOIN_GRANT_TABLES );
        query.append( AGENDA_TABLE_USER_PERMISSION_CONDITION );

        /** Only events */
        query.append( AGENDA_TABLE_TYPE_CONDITION );

        query.append( String.format( AGENDA_TABLE_MIN_DATE_CONDITION, sParameter) );

        Optional
            .ofNullable(maxDate)
            .ifPresent( date -> query.append( String.format( AGENDA_TABLE_MAX_DATE_CONDITION, sParameter) ));

        /**
         * Set parameters, the first one the user id parameter
         * And the other one, they have been referenced as the hash of the object
         */
        return Optional
            .ofNullable( query )
            .map( StringBuilder::toString)
            .map( ThreadLocalManager.getRecursiveTransaction()::createQuery )
            // Set user parameter
            .map( q ->  q.setParameter( SQL_USER_ID_PARAMETER, userBean.getId() ) )
            .map( q -> q.setParameter( SQL_AGENDA_TYPE_PARAMETER, EVENT.ordinal() )  )
            .map( q -> setDates( q , minDate , maxDate ) )
            // Add other conditions
            // Get result
            .map( Query::getResultList)
            .orElse( null );

    }

    /**
     * Set dates in query
     * @param query the SQL query
     * @param minDate the min date
     * @param maxDate the max date
     * @return the query
     */
    private static  Query setDates( Query query ,  Date minDate, Date maxDate)
    {
        query.setParameter( SQL_AGENDA_MIN_DATE_PARAMETER, minDate);

        return Optional
            .ofNullable(maxDate)
            .map( date -> query.setParameter( SQL_AGENDA_MAX_DATE_PARAMETER, maxDate ))
            .orElse( query);

    }

    /**
     * Get agenda events
     * @return
     */
    public static List<AgendaBean> getAgenda(){
        return getAgendaEvents( new HashMap<>() );
    }

    /**
     * Get agenda event by id
     * @param m_eventId the agenda id
     * @return a list of agenda events
     */
    public static AgendaBean getAgendaById( Long m_eventId ){
        HashMap<String,Object> conditions = new HashMap<>();
        conditions.put( AGENDA_EVENT_ID_COLUMN_NAME , m_eventId );

        ServiceLogger.info( AgendaDao.class , "GET AGENDA BY ID " + m_eventId );

        return Optional
            .ofNullable( conditions )
            .map(AgendaDao::getAgendaEvents)
            .map(List::iterator)
            .filter( Iterator::hasNext )
            .map(Iterator::next)
            .orElse(null);
    }

    /**
     * Get agenda event by parent id
     * @param m_eventId the agenda parent id
     * @return a list of agenda events
     */
    public static List<AgendaBean> getAgendaByParentId( Long m_eventId ){
        HashMap<String,Object> conditions = new HashMap<>();
        conditions.put( AGENDA_EVENT_PARENT_ID_COLUMN_NAME, m_eventId );

        ServiceLogger.info( AgendaDao.class , "GET AGENDA BY PARENT ID " + m_eventId );

        return getAgendaEvents( conditions );
    }

    /**
     * Get car accidents list
     * @return a list of agenda car accidents
     */
    public static List<CarAccidentBean> getCarAccidentList( ) {
        HashMap<String,Object> conditions = new HashMap<>();
        conditions.put( AGENDA_EVENT_TYPE_COLUMN_NAME , CAR_ACCIDENT.ordinal() );

        List<AgendaBean> hCarAccidents = getAgendaEvents(  conditions );

        return ObjectDao.getCarInfoBeans(hCarAccidents, CarAccidentBean::new, AgendaBean::getPolicyId);
    }

    /**
     * Get daily events
     * @return a list of daily events
     */
    public static List<AgendaBean> getDailyEvents( ) {

        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        LocalDate tomorrow = today.plusDays(ONE_DAY);

        LocalDateTime todayMidnight = LocalDateTime.of(today, MIDNIGHT );
        LocalDateTime tomorrowMidnight = LocalDateTime.of(tomorrow, MIDNIGHT );

        return getEvents(  todayMidnight , tomorrowMidnight , AGENDA_EVENT_START_DATE_COLUMN_NAME  );
    }


    /**
     * Get daily events
     * @return a list of daily events
     */
    public static List<AgendaBean> getMonthLyEvents( ) {

        LocalDate firstDayOfCurrentMonth = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
        LocalDate firstDayOfNextMont = LocalDate.now().with(TemporalAdjusters.firstDayOfNextMonth());

        LocalDateTime firstDayOfCurrentMonthMidNight = LocalDateTime.of(firstDayOfCurrentMonth, MIDNIGHT);
        LocalDateTime firstDayOfNextMontMidNight = LocalDateTime.of(firstDayOfNextMont, MIDNIGHT);

        return getEvents( firstDayOfCurrentMonthMidNight , firstDayOfNextMontMidNight , AGENDA_EVENT_START_DATE_COLUMN_NAME  );
    }

    /**
     * Get last events from a date
     * @param lastDate the last date
     * @return the events from this date
     */
    public static List<AgendaBean> getLastEvents( Long lastDate ) {

        LocalDateTime lastDateTime = Instant.ofEpochMilli( lastDate )
            .atZone( ZoneId.systemDefault() )
            .toLocalDateTime();

        return getEvents( lastDateTime , null , AGENDA_EVENT_UPDATE_DATE_COLUMN_NAME  );
    }


    /**
     * Add agenda bean
     * @param agendaBean the agenda bean to be added
     * @return the agenda bean added or null in case of error
     */
    public static AgendaBean addAgendaBean( AgendaBean agendaBean )
        throws Throwable {

        /** If event has parent id , then verify if user has permissions to add the event under this parent */
        Optional
            .ofNullable( agendaBean )
            .filter( GrantsDao::hasAuthorizationOnCreateAgendaItem)
            .orElseThrow(() -> new RestServiceException("User has no authorization on create " +
                AgendaItemType.getType( agendaBean.getType() ) , USER_NO_AUTHORIZED ) );

        /** Set random id in agenda bean */
        Optional
            .ofNullable( agendaBean )
            .ifPresent( bean -> bean.setId( m_autoIncrementProvider.getId() ));

        Date now = new Date();

        /** Set start date just now */
        Optional
            .ofNullable(agendaBean)
            .ifPresent( bean -> bean.setStartDate( now ));

        RecursiveTransaction transaction = ThreadLocalManager.getRecursiveTransaction();

        try
        {
            /** Add permissions */
            transaction.begin();
            GrantsDao.addGrants( agendaBean );
            transaction.persist( agendaBean );
            transaction.commit();
        }catch ( Exception e ) {
            transaction.rollback();
            throw e;
        }

        /** Set update time of agenda and parents */
        setUpdateTime( agendaBean.getId() , now );

        ServiceLogger.info( AgendaDao.class , ADD , agendaBean  );

        return agendaBean;
    }

    /**
     * Update agenda bean
     * @param lAgendaId the agenda id
     * @throws Throwable the exetpion
     */
    public static void setUpdateTime( Long lAgendaId )
    throws Throwable {
        setUpdateTime( lAgendaId , new Date());
    }

    /**
     * Update agenda bean
     * @param lAgendaId the agenda id
     * @return the agenda bean
     */
    public static void setUpdateTime( Long lAgendaId , Date date )
    throws Throwable {
        if( lAgendaId == null ) {
            return;
        }

        RecursiveTransaction transaction = ThreadLocalManager.getRecursiveTransaction();

        AgendaBean agendaBean = transaction.find( AgendaBean.class , lAgendaId );
        if( agendaBean == null ){
            return;
        }

        try {
            transaction.begin();
                agendaBean.setUpdateDate( date );
                setUpdateTime( agendaBean.getParentId() , date );
                transaction.merge(agendaBean);
            transaction.commit();
        }catch ( Throwable e ) {
            transaction.rollback();
            throw e;
        }
    }

    /**
     * Update agenda bean
     * @param agendaBean the agenda bean to be added
     * @return the agenda bean added or null in case of error
     */
    public static AgendaBean updateAgendaBean( AgendaBean agendaBean )
        throws Throwable{
        /** Verify if user can modify the agenda event */
        if (GrantsDao.hasAuthorizationOnAgendaItem(agendaBean) == false) {
            throw new RestServiceException("User has no authorization update agenda ", USER_NO_AUTHORIZED);
        }
        Date now = new Date();

        /** If closing the agenda bean ( exists the end date ) then the server set the end date */
        Optional
            .ofNullable( agendaBean )
            .map( AgendaBean::getEndDate )
            .filter( endDate -> endDate != null )
            .ifPresent( endDate -> agendaBean.setEndDate( now ));

        ThreadLocalManager.getRecursiveTransaction().merge(agendaBean);

        /** Set update time of agenda and parents */
        setUpdateTime( agendaBean.getId() , now );

        ServiceLogger.info( AgendaDao.class , UPDATE , agendaBean );

        return agendaBean;
    }


    /**
     * Update agenda bean
     * @param agendaId the agenda id to be removed
     * @return the agenda bean removed or exception in case of error
     */
    public static AgendaBean removeAgendaBean( Long agendaId )
        throws Throwable{

        AgendaBean agendaBean;

        // Verify if user is insurance staff
        if ( ThreadLocalManager.getUserBean().getRole() != UserBean.Role.INSURANCE_STAFF.id() ) {
            throw new RestServiceException("User has no authorization remove agenda", USER_NO_AUTHORIZED);
        }

        RecursiveTransaction transaction = ThreadLocalManager.getRecursiveTransaction();
        transaction.begin();

        List<AgendaBean> hChildren = Optional
            .ofNullable( agendaId )
            .map( AgendaDao::getAgendaByParentId )
            .orElseGet( ArrayList::new );

         try {
            for ( AgendaBean child : hChildren ){
                removeAgendaBean( child.getId() );
            }
         }
         catch( Exception e ) {
             transaction.rollback();
             LoggerFactory.getLogger( AgendaBean.class ).error("Error removing agenda" , e );
             throw e;
         }

        agendaBean = Optional
            .ofNullable( agendaId )
            .map( id -> transaction.find( AgendaBean.class , id ) )
            .orElse( null );

        /** Remove documents from directory */
        DocumentDao.removeDocuments( agendaId );

        /** Remove damages if agenda item is a car accident */
        if( agendaBean.getType() == CAR_ACCIDENT.ordinal() ) {
            DamageDao.removeDamages(agendaId);
        }

        transaction.remove( agendaBean );

        transaction.commit();

        if( agendaBean != null ){
            ServiceLogger.info( AgendaDao.class , REMOVE , agendaBean );
        }
        else {
            ServiceLogger.error( AgendaDao.class , REMOVE , agendaBean ,"Agenda bean doesn't exist" );
        }

        return Optional
            .ofNullable( agendaBean )
            .orElseThrow( () -> new RestServiceException("Resource doesn't exist", RESOURCE_NOT_FOUND) );
    }
}
