package es.uoc.car.accident.manager.server.jpa.daos;

import java.util.Date;

/**
 * This class provide an id for database insertions
 */
public class AutoIncrementProvider {

    /** The id */
    public Long m_lId;

    /**
     * Constructor
     */
    public AutoIncrementProvider() {
        this.m_lId = new Date().getTime();
    }

    /**
     * Get a new id
     * @return the new id
     */
    public Long getId() {
        return m_lId++;
    }
}
