package es.uoc.car.accident.manager.server.json.services;

import static es.uoc.car.accident.manager.server.constants.CarAccidentServiceConstants.USER_NO_AUTHORIZED;

public class RestServiceException extends  Exception {

    /** The restful service status to return user */
    private Integer m_iStatus;

    /**
     * Constructor
     * @param message the exception description message
     * @param iStatus the status to return to the user
     */
    public RestServiceException(String message, Integer iStatus ) {
        super(message);
        this.m_iStatus = iStatus;
    }

    /**
     * Get status
     * @return the status
     */
    public Integer getStatus() {
        return m_iStatus;
    }

    /**
     * Get status
     * @param e the exception
     * @return the status
     */
    public static Integer getStatus( Throwable e ) {
        if( e instanceof RestServiceException )
        {
            return ( ( RestServiceException) e ).getStatus();
        }

       return  USER_NO_AUTHORIZED;
    }
}
