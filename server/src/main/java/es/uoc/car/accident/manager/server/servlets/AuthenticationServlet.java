package es.uoc.car.accident.manager.server.servlets;

import es.uoc.car.accident.manager.server.jpa.beans.PolicyBean;
import es.uoc.car.accident.manager.server.jpa.beans.UserBean;
import es.uoc.car.accident.manager.server.jpa.beans.extended.CarInfoBean;
import es.uoc.car.accident.manager.server.jpa.daos.ObjectDao;
import es.uoc.car.accident.manager.server.jpa.daos.PolicyDao;
import es.uoc.car.accident.manager.server.thread.local.manager.ThreadLocalManager;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.util.*;

import static es.uoc.car.accident.manager.server.constants.CarAccidentServiceConstants.*;
import static org.eclipse.persistence.jaxb.JAXBContextFactory.createContext;

public class AuthenticationServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        /** Get user and password from request */
        String sPassword = Optional
                .ofNullable( PASSWORD_LABEL )
                .map( request::getParameter )
                .orElse( null );

        String sLogin = Optional
                .ofNullable( LOGIN_LABEL )
                .map( request::getParameter )
                .orElse( null );

        UserBean userBean = Optional
                .ofNullable( sPassword )
                .filter( password -> sLogin != null )
                .map(  password -> getUserBean( sLogin , sPassword ) )
                .orElse( null );

        /** If no user bean throw servlet exception */
        if( userBean == null ) {
            LoggerFactory.getLogger( AuthenticationServlet.class ).error( "Bad user o bad password" );
            throw new ServletException("No user or bad password");
        }

        /** Create session and set user bean as an attribute
         *  A cookie is sent to client , this cookie will be used to
         *  authenticated it in all the next request
         **/
        Optional
            .ofNullable( request )
            .map( HttpServletRequest::getSession )
            .ifPresent( session -> session.setAttribute( USER_SESSION_ATTRIBUTE , userBean ) );

        /** Send user bean */
        try{
            sendUserBean( response , userBean );
            LoggerFactory.getLogger( AuthenticationServlet.class ).info( "USER LOGIN\n{}" , userBean );
        }
        catch ( IOException | JAXBException e ) {
            LoggerFactory.getLogger( AuthenticationServlet.class ).error( "Bad user o bad password\n" + userBean , e );
            throw new ServletException( e );
        }
    }

    /**
     * Send user bean as a JSON object
     * @param response the servlet response
     * @param userBean the user bean
     * @throws IOException io exception
     * @throws JAXBException jaxb exception
     */
    public static void sendUserBean( HttpServletResponse response , UserBean userBean  )
            throws IOException, JAXBException  {

        PolicyBean policy = PolicyDao.getUserPolicy( userBean.getId() );

        CarInfoBean carInfoBean = new CarInfoBean( policy , userBean );

        // Actual logic goes here.
        response.setContentType(APPLICATION_JSON);

        Map<String, Object> properties = new HashMap<String, Object>(1);
        properties.put(MarshallerProperties.JSON_WRAPPER_AS_ARRAY_NAME, false);

        JAXBContext jc = createContext(new Class[] {CarInfoBean.class}, properties);
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, APPLICATION_JSON);
        marshaller.marshal( carInfoBean, response.getWriter() );
    }

    /**
     * Get user bean in case the login and password matches the data stored in database
     * @param sLogin the user login
     * @param sPassword the user password
     * @return
     */

    public UserBean getUserBean( String sLogin , String sPassword ){

        List<UserBean> hUsers = ObjectDao.getObjectList(
            SELECT_USER_FROM_USERS_TABLE,
            USERS_TABLE_USER_LOGIN_CONDITION,
            SQL_USER_LOGIN_PARAMETER,
            sLogin );

        return Optional
                .ofNullable( hUsers )
                .filter( query -> sPassword != null )
                .filter( users -> users.size() == 1 )
                .map( List::iterator )
                .map( Iterator::next )
                .filter( user -> user.getPassword() != null )
                .filter( user -> user.getPassword().equalsIgnoreCase( sPassword ) )
                .orElse( null );
    }
}