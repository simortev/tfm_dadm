package es.uoc.car.accident.manager.server.jpa.beans;

import es.uoc.car.accident.manager.server.jpa.beans.interfaces.IObjectBean;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

@Entity
@Cacheable(false)
@Table(name = "GRANTS")
public class GrantBean implements IObjectBean,Serializable {

    /** ID of the event in agenda table */
    @Id
    @Column(name="ID")
    private Long m_lID;

    /** User id */
    @Column(name="ID_USER")
    private Long m_lUserId;

    /** User id */
    @Column(name="ID_AGENDA")
    private Long m_lAgendaId;

    /**
     * Constructor
     */
    public GrantBean() {
    }

    /**
     * Get Id
     * @return the grant id
     */
    @Override
    @XmlTransient
    public Long getId() {
        return m_lID;
    }

    /**
     * Get user id
     * @return the user id
     */
    @XmlTransient
    public Long getUserId() {
        return m_lUserId;
    }

    /**
     * Get agenda id
     * @return the agenda id
     */
    @XmlTransient
    public Long getAgendaId() {
        return m_lAgendaId;
    }

    /**
     * Create a grant bean from its properties
     * @param ID the grant id
     * @param userId the user id
     * @param agendaId the agenda id
     * @return the grant bean
     */
    public static GrantBean createGrantBean( Long ID , Long userId , Long agendaId ) {

        GrantBean grantBean = new GrantBean();
        grantBean.m_lAgendaId = agendaId;
        grantBean.m_lID = ID;
        grantBean.m_lUserId = userId;

        return grantBean;
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();

        builder.append( "GRANT: ").append( m_lID);
        builder.append( " AGENDA ID: " ).append(m_lAgendaId);
        builder.append(" USER ID: ").append( m_lUserId );

        return builder.toString();
    }
}
