package es.uoc.car.accident.manager.server.jpa.beans.extended;

import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import es.uoc.car.accident.manager.server.jpa.beans.interfaces.IObjectBean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Optional;

/**
 * The car accident bean, used when the list is requested
 */
public class CarAccidentBean extends CarInfoBean implements IObjectBean {

    @XmlElement(name="agenda")
    private AgendaBean m_agenda;

    /**
     * Constructor
     */
    public CarAccidentBean() {

    }

    /**
     * Constructor
     * @param agenda the car accident agenda bean
     * @param carInfoBean the car info bean
     */
    public CarAccidentBean(AgendaBean agenda, CarInfoBean carInfoBean ) {
        super( carInfoBean);
        m_agenda = agenda;
    }

    @XmlTransient
    public AgendaBean getCarAccident() {
        return m_agenda;
    }

    @Override
    public Long getId() {
        return Optional.ofNullable(m_agenda).map(AgendaBean::getId).orElse(null);
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();

        builder.append( super.toString() ).append( "\n");

        Optional
            .ofNullable( m_agenda )
            .ifPresent( carInfoBean -> builder.append( m_agenda ).append( "\n") );

        return builder.toString();
    }

}
