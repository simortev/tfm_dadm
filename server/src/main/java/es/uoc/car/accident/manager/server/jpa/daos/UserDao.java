package es.uoc.car.accident.manager.server.jpa.daos;

import es.uoc.car.accident.manager.server.jpa.beans.UserBean;

import java.util.List;
import java.util.Map;

import static es.uoc.car.accident.manager.server.constants.CarAccidentServiceConstants.*;

public class UserDao {
    /**
     * To avoid this class can be instantiated
     */
    private UserDao() {

    }

    /**
     * Get users
     * @param hUsers a collection of users
     * @return map of users
     */
    public static Map<Long,UserBean> getUsers(List<Long> hUsers){
        return ObjectDao.getObjectMap(
            SELECT_USER_FROM_USERS_TABLE,
            USERS_TABLE_USER_LIST_ID_CONDITION,
            SQL_USER_ID_LIST_PARAMETER,
            hUsers);

    }
}
