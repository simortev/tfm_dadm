package es.uoc.car.accident.manager.server.json.services;

import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import es.uoc.car.accident.manager.server.jpa.beans.extended.CarAccidentBean;
import es.uoc.car.accident.manager.server.jpa.daos.AgendaDao;
import es.uoc.car.accident.manager.server.log.ServiceLogger;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

/**
 * This class provides all the RestFul services for agenda data
 *
 * Car accidents, tasks and event ( all are agenda events )
 *
 * A car accident doesn't have a parent event, a task has a car accident parent , and an event has a
 * task parent
 *
 * The services verify if user has permissions to access it
 *
 *   + Get all the events that a user can acess
 *   + Get event by event id
 *   + Get children events of a parent ( by parent event id )
 *   + Get car accident events of a user
 *   + Add event
 *   + Update event
 *   + Remove event by id
 *
 *   In get method if user doesn't have permissions , the list will be empty
 *
 *   For add, update or remove an HTTP error status will be returned
 */

@Path("/agenda")
public class AgendaService extends ServiceLogger {

    /**
     * Get only an agenda event of the day
     * @return the event or null
     */
    @GET
    @Path("/element/daily")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDailyEvents() {
        GenericEntity entity = new GenericEntity<List<AgendaBean>>(AgendaDao.getDailyEvents()) {};
        return Response.ok(entity).build();
    }

    /**
     * Get only an agenda event of the month
     * @return the event or null
     */
    @GET
    @Path("/element/monthly")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMonthlyEvents() {
        GenericEntity entity = new GenericEntity<List<AgendaBean>>(AgendaDao.getMonthLyEvents()) {};
        return Response.ok(entity).build();
    }


    /**
     * Get only last agenda events
     * @return the event or null
     */
    @GET
    @Path("/element/last/{last_date}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLastEvents( @PathParam("last_date")  Long lastDate ) {
        GenericEntity entity = new GenericEntity<List<AgendaBean>>(AgendaDao.getLastEvents( lastDate )) {};
        return Response.ok(entity).build();
    }

    /**
     * Get only an agenda event by its id ( if user has permission )
     * @param id the id of the agenda event
     * @return the event or null
     */
    @GET
    @Path("/element/{agenda_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAgendaEventById(@PathParam("agenda_id") Long id) {

        AgendaBean agendaBean = AgendaDao.getAgendaById( id );

        if( agendaBean != null ) {
            GenericEntity entity = new GenericEntity<AgendaBean>(agendaBean) {};
            return Response.ok(entity).build();
        }
        else{
            return Response.status(Response.Status.NOT_FOUND ).build();
        }
    }

    /**
     * Get the events of a parent  ( if user has permission )
     * @param parentId the parent id
     * @return a list of child events
     */
    @GET
    @Path("/agenda/{agenda_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAgendaEventByParentId(@PathParam("agenda_id") Long parentId) {
        GenericEntity entity = new GenericEntity<List<AgendaBean>>(AgendaDao.getAgendaByParentId(parentId)) {};
        return Response.ok(entity).build();
    }

    /**
     * List of events without any parent when the user has permission ( these are car accidents )
     * @return the list of events with no parent ( car accident )
     */
    @GET
    @Path("/accidents")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCarAccidents() {
        GenericEntity entity = new GenericEntity<List<CarAccidentBean>>(AgendaDao.getCarAccidentList()) {};
        return Response.ok(entity).build();
    }

    /**
     * Add an agenda bean , if user has permission on the parent event
     * the permissions on this event is granted to all the users who have permission to access parent
     * @param agendaBean the agenda bean to be added
     * @return the response with the agenda bean added
     */
    @POST
    @Path("/element")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addAgendaEvent( AgendaBean agendaBean ) {
        try {
            return Response.ok().entity( AgendaDao.addAgendaBean( agendaBean ) ).build();
        }
        catch ( Throwable e ){
            m_log.error( "ERROR: {}" , e.getMessage() );
            return Response.status( RestServiceException.getStatus( e ) ).entity( agendaBean ).build();
        }
    }

    /**
     * Update an agenda bean , if user has permission on the  event
     * @param agendaBean the agenda bean to be updated
     * @return the response with the agenda bean updated
     */
    @PUT
    @Path("/element")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateAgendaEvent( AgendaBean agendaBean ) {

        try {
            return Response.ok().entity( AgendaDao.updateAgendaBean( agendaBean ) ).build();
        }
        catch ( Throwable e ){
            m_log.error( "ERROR" , e);
            return Response.status( RestServiceException.getStatus( e )).entity( agendaBean ).build();
        }
    }

    /**
     * Remove an event ( if user has permission )
     * @param agendaId the event id
     * @return the event removed
     */
    @DELETE
    @Path("/element/{agenda_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAgendaEvent(@PathParam("agenda_id") Long agendaId) {
        try {
            return Response.ok().entity( AgendaDao.removeAgendaBean( agendaId ) ).build();
        }
        catch ( Throwable e ){
            m_log.error( "ERROR: {}" , e.getMessage() );
            return Response.status( RestServiceException.getStatus( e ) ).build();
        }
    }
}
