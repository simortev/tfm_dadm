package es.uoc.car.accident.manager.server.json.services;


import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;


/**
 * This class is aimed to avoid invoked several times begin transaction and commits in different DAO
 * in the same service request, executing then in the same thread
 */
public class RecursiveTransaction {

    /** The entity manager */
    private EntityManager m_em;

    /** The number of times the begin command has been invoked */
    private int m_iLevels;

    /**
     * Constructor
     * @param em the entity manager
     */
    public RecursiveTransaction( EntityManager em) {
        m_em = em;
        m_iLevels = 0;
    }

    /**
     * Begin transaction
     */
    public void begin() {
        if( m_iLevels == 0){
            m_em.getTransaction().begin();
        }

        m_iLevels++;
    }

    /**
     * Commit transaction
     */
    public void commit() {
        m_iLevels = m_iLevels > 0 ? m_iLevels - 1: 0;

        if(m_iLevels == 0 ) {
            m_em.getTransaction().commit();
        }
    }

    /**
     * Execute roll back
     */
    public void rollback() {
        m_iLevels = m_iLevels > 0 ? m_iLevels - 1: 0;

        if(m_iLevels == 0 ) {
            m_em.getTransaction().rollback();
        }
    }

    /**
     * persist object
     * @param object the object to be persisted
     */
    public void persist( Object object ) throws Throwable  {
        execute( () -> m_em.persist(object));
    }

    /**
     * Persist a list of objects
     * @param hObject the list of objects to persist
     */
    public<T>  void persist( List<T> hObject ) throws Throwable  {
        execute( () -> hObject.forEach( m_em::persist ) );
    }

    /**
     * merge object
     * @param object the object to be merged
     */
    public void merge( Object object ) throws Throwable  {
        execute( () -> m_em.merge(object));
    }

    /**
     * remove object
     * @param object the object to be removed
     */
    public void remove( Object object ) throws Throwable  {
        execute( () -> m_em.remove(object));
    }


    /**
     * execute opreation
     */
    private void execute( Runnable operation )
        throws Throwable {
        begin();
        try {
            operation.run();
            commit();
        }catch ( Throwable e ){
            rollback();
            throw new Throwable( e );
        }
    }

    /**
     * Find object by primary key
     * @param cl the object class
     * @param primaryKey the primary key
     * @param <T> the type of object
     */
    public<T> T find( Class<T> cl , Object primaryKey) {
        return m_em.find( cl , primaryKey);
    }

    /**
     * Create a query
     * @param sqlQuery the query to be created
     */
    public Query createQuery(String sqlQuery ) {
        return m_em.createQuery(sqlQuery );
    }

    /**
     * Clear transaction
     */
    public void clear() {
        m_iLevels = 0;
        m_em.clear();
    }
}
