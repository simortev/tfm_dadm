package es.uoc.car.accident.manager.server.constants;

/**
 * This class contains all the constants used in more than one class of car management
 * application
 */
public class CarAccidentServiceConstants {

    /****************** HTTP STATUS **********************************/

    /** Bad request */
    public final static int BAD_REQUEST = 404;

    /** User no authorized to do this action */
    public final static int USER_NO_AUTHORIZED = 403;

    /** Resource not found */
    public final static int RESOURCE_NOT_FOUND = 404;

    /****************** HTTP CONSTANTS **********************************/

    /** The password label */
    public final static String PASSWORD_LABEL = "password";

    /** The login label */
    public final static String LOGIN_LABEL = "login";

    /** Media type application json */
    public final static String APPLICATION_JSON = "application/json";

    /** The content disposition header */
    public final static String CONTENT_DISPOSITION_HEADER = "Content-disposition";

    /** User session attribute */
    public final static String USER_SESSION_ATTRIBUTE = "USER_SESSION_ATTRIBUTE";

    /** The attachment file name format*/
    public final static String ATTACHMENT_FILENAME_FORMAT = "attachment; filename =%s";

    /************************** COLUMN NAMES ************************************************/

    /** Agenda event id column name */
    public final static String AGENDA_EVENT_ID_COLUMN_NAME = "event.m_lID";

    /** Agenda event parent id column name */
    public final static String AGENDA_EVENT_PARENT_ID_COLUMN_NAME = "event.m_lParentId";

    /** Agenda event type column name */
    public final static String AGENDA_EVENT_TYPE_COLUMN_NAME = "event.m_iType";

    /** The agenda date column parameter */
    public final static String AGENDA_EVENT_DATE_COLUMN_NAME_FORMAT = "event.%s";

    /** The start date column*/
    public final static  String AGENDA_EVENT_START_DATE_COLUMN_NAME = "m_startDate";

    /** The update date column */
    public final static  String AGENDA_EVENT_UPDATE_DATE_COLUMN_NAME = "m_updateDate";

    /** The damage agenda id */
    public final static String DAMAGE_AGENDA_ID_COLUMN_NAME = "damages.m_lAgendaId";

    /******************** PARAMETERS ID ****************************************************/

    /** SQL User login parameter*/
    public final static String SQL_USER_LOGIN_PARAMETER = "login";

    /** SQL user role id parameter ss*/
    public final static String SQL_USER_ROLE_ID_PARAMETER = "roleId";

    /** SQL User id parameter*/
    public final static String SQL_USER_ID_PARAMETER = "userId";

    /** SQL User id parameter*/
    public final static String SQL_USER_ID_LIST_PARAMETER = "userIdList";

    /** SQL agenda id parameter */
    public final static String SQL_AGENDA_ID_PARAMETER = "agendaId";

    /** The agenda type */
    public final static String SQL_AGENDA_TYPE_PARAMETER = "agendaType";

    /** The agenda min date */
    public final static String SQL_AGENDA_MIN_DATE_PARAMETER = "agendaMinDate";

    /** The agenda max date*/
    public final static String SQL_AGENDA_MAX_DATE_PARAMETER = "agendaMaxDate";

    /** SQL document id parameter */
    public final static String SQL_DOCUMENT_ID_PARAMETER = "documentId";

    /** SQL Policy id parameter*/
    public final static String SQL_POLICY_ID_LIST_PARAMETER = "policyIdList";

    public final static String SQL_POLICY_NUMBER_PARAMETER = "policyNumber";


    /** Name of the parameter in an additional condition*/
    public final static String ADDITIONAL_CONDITION_PARAMETER_NAME_FORMAT = "COLUMN_%d";

    /******************** SQL SELECT STATEMENT *******************************/


    /*** USER TABLE */

    /** The SQL query for request the user with a login */
    public final static String SELECT_USER_FROM_USERS_TABLE = "SELECT user FROM UserBean as user";

    /** User table login condition */
    public final static String USERS_TABLE_USER_LOGIN_CONDITION =
        " where user.m_sLogin = :" + SQL_USER_LOGIN_PARAMETER;

    /** User table role id condition */
    public final static String USERS_TABLE_USER_ROLE_CONDITION =
        " where user.m_iRole = :" + SQL_USER_ROLE_ID_PARAMETER;

    /** User list */
    public final static String USERS_TABLE_USER_LIST_ID_CONDITION =
        " where user.m_lID in :" + SQL_USER_ID_LIST_PARAMETER;

    /** AGENDA TABLE */

    /** Select form agenda sql statement agenda join grant tables */
    public final static String SELECT_EVENT_FROM_AGENDA_JOIN_GRANT_TABLES =
        "SELECT event FROM AgendaBean as event , GrantBean as grant";

    /** Select from agenda grant condition , agenda id equal to event id and user id equal to user session idss*/
    public final static String AGENDA_TABLE_USER_PERMISSION_CONDITION =
        " where event.m_lID = grant.m_lAgendaId and grant.m_lUserId = :" + SQL_USER_ID_PARAMETER;

    /** Condition with the format table.column =:parameter */
    public final static String ADDITIONAL_CONDITION_FORMAT =
        " and %s = :" + ADDITIONAL_CONDITION_PARAMETER_NAME_FORMAT;

    public final static String AGENDA_TABLE_TYPE_CONDITION = " and " + AGENDA_EVENT_TYPE_COLUMN_NAME
        + " = :" + SQL_AGENDA_TYPE_PARAMETER;


    public final static String AGENDA_TABLE_MIN_DATE_CONDITION = " and " + AGENDA_EVENT_DATE_COLUMN_NAME_FORMAT
        + " >= :" + SQL_AGENDA_MIN_DATE_PARAMETER;

    public final static String AGENDA_TABLE_MAX_DATE_CONDITION = " and " + AGENDA_EVENT_DATE_COLUMN_NAME_FORMAT
        + " < :" + SQL_AGENDA_MAX_DATE_PARAMETER;


    /** GRANT TABLE */

    /** Select from grants table sql statement*/
    public final static String SELECT_GRANT_FROM_GRANTS_TABLE = "SELECT grant FROM GrantBean as grant";

    /** Agenda id match condition */
    public final static String GRANT_TABLE_AGENDA_ID_CONDITION =
        " where grant.m_lAgendaId = :" + SQL_AGENDA_ID_PARAMETER;

    /** User id condition */
    public final static String GRANT_TABLE_USER_ID_CONDITION =
        " and grant.m_lUserId = :" + SQL_USER_ID_PARAMETER;

    /** DAMAGE TABLE */

    /** Select from damages table */
    public final static String SELECT_DAMAGE_FROM_DAMAGES_TABLE = "SELECT damages FROM DamageBean as damages";

    /** Agenda id match condition */
    public final static String DAMAGES_TABLE_AGENDA_ID_CONDITION =
        " where damages.m_lAgendaId = :" + SQL_AGENDA_ID_PARAMETER;

    /** DOCUMENT TABLE */

    /** Select from grants table sql statement*/
    public final static String SELECT_DOCUMENT_FROM_DOCUMENTS_TABLE = "SELECT document FROM DocumentBean as document";

    /** Agenda id match condition */
    public final static String DOCUMENT_TABLE_AGENDA_ID_CONDITION =
        " where document.m_lAgendaId = :" + SQL_AGENDA_ID_PARAMETER;

    /** Document id match condition */
    public final static String DOCUMENT_TABLE_DOCUMENT_ID_CONDITION =
        " where document.m_lID = :" + SQL_DOCUMENT_ID_PARAMETER;

    /***  POLICY TABLE **/

    /** Select policy from table policy */
    public final static String SELECT_POLICY_FROM_POLICIES_TABLE =
        "SELECT policy FROM PolicyBean as policy";

    /** Policy list */
    public final static String POLICY_TABLE_POLICY_LIST_ID_CONDITION =
        " where policy.m_lID in :" + SQL_POLICY_ID_LIST_PARAMETER;

    /** Policy list */
    public final static String POLICY_TABLE_USER_ID_CONDITION =
        " where policy.m_lUserId = :" + SQL_USER_ID_PARAMETER;

    /** Policy list */
    public final static String POLICY_TABLE_POLICY_NUMBER_CONDITION =
        " where policy.m_sPolicyNumber = :" + SQL_POLICY_NUMBER_PARAMETER;

    /****************** PROPERTIES **********************************/

    public final static String DOCUMENT_DIRECTORY = "document_directory";

    /**
     * Private constructor avoid to be instantiated
     */
    private CarAccidentServiceConstants() {

    }
}
