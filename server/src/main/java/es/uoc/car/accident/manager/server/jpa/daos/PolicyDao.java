package es.uoc.car.accident.manager.server.jpa.daos;


import es.uoc.car.accident.manager.server.jpa.beans.PolicyBean;

import java.util.*;

import static es.uoc.car.accident.manager.server.constants.CarAccidentServiceConstants.*;

public class PolicyDao {
    /**
     * To avoid this class can be instantiated
     */
    private PolicyDao() {

    }

    /**
     * Get policy by policy id
     * @param lPolicyId the policy id
     * @return the policy bean
     */
    public static PolicyBean getPolicy(Long lPolicyId){
        return Optional
            .ofNullable(lPolicyId)
            .map(Arrays::asList)
            .map(PolicyDao::getPolicies)
            .map(Map::values)
            .map(Collection::iterator)
            .filter(Iterator::hasNext)
            .map(Iterator::next)
            .orElse(null );
    }

    /**
     * Get policy by user id
     * @param lUserId the user id
     * @return the policy bean
     */
    public static PolicyBean getUserPolicy(Long lUserId){
        return Optional
            .ofNullable(lUserId)
            .map(PolicyDao::getUserPolicies)
            .map(Collection::iterator)
            .filter(Iterator::hasNext)
            .map(Iterator::next)
            .orElse(null );
    }

    /**
     * Get policies
     * @param hPolicies a collection of policies
     * @return map of policies
     */
    public static Map<Long,PolicyBean> getPolicies(List<Long> hPolicies){
        return ObjectDao.getObjectMap(
            SELECT_POLICY_FROM_POLICIES_TABLE,
            POLICY_TABLE_POLICY_LIST_ID_CONDITION,
            SQL_POLICY_ID_LIST_PARAMETER,
            hPolicies);
    }

    /**
     * Get policies
     * @param lUserId the user id
     * @return a list of policies
     */
    public static List<PolicyBean> getUserPolicies(Long lUserId ){
        return ObjectDao.getObjectList(
            SELECT_POLICY_FROM_POLICIES_TABLE,
            POLICY_TABLE_USER_ID_CONDITION,
            SQL_USER_ID_PARAMETER,
            lUserId);
    }

    /**
     * Get policy with this policy number
     * @param sPolicyNumber the user id
     * @return a list of policies
     */
    public static PolicyBean getPolicyByPolicyNumber(String sPolicyNumber ){
        List<PolicyBean>  hPolicies =  ObjectDao.getObjectList(
            SELECT_POLICY_FROM_POLICIES_TABLE,
            POLICY_TABLE_POLICY_NUMBER_CONDITION,
            SQL_POLICY_NUMBER_PARAMETER,
            sPolicyNumber);

        return Optional
            .ofNullable(hPolicies)
            .orElseGet(ArrayList::new)
            .stream()
            .findFirst()
            .orElse(null );
    }
}
