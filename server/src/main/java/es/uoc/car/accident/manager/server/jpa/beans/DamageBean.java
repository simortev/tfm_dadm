package es.uoc.car.accident.manager.server.jpa.beans;

import es.uoc.car.accident.manager.server.jpa.beans.interfaces.IObjectBean;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Optional;

/**
 * The damage bean
 */
@Entity
@Cacheable(false)
@Table(name = "DAMAGES")
public class DamageBean implements IObjectBean {

    /** ID of the event in agenda table */
    @Id
    @Column(name="ID")
    @XmlElement(name="id")
    private Long m_lID;

    /** Agenda id */
    @Column(name="ID_AGENDA")
    @XmlElement(name="agenda_id")
    private Long m_lAgendaId;

    /** The policy id related with the damage, the car model, ... */
    @Column(name="ID_POLICY")
    @XmlElement(name="policy_id")
    private Long m_lPolicyId;

    /** The damages of the accident */
    @Column(name="DAMAGES")
    @XmlElement(name="damages")
    private String m_sDamages;

    @Override
    public Long getId() {
        return m_lID;
    }

    /**
     * Set id
     * @param lId set id
     */
    public void setId( Long lId) {
        m_lID = lId;
    }

    /**
     * Get agenda id
     * @return the agenda id
     */
    @XmlTransient
    public Long getAgendaId() {
        return m_lAgendaId;
    }

    /**
     * Set agenda id
     * @param lAgendaId the agenda id
     */
    public void setAgendaId( Long lAgendaId ) {
        m_lAgendaId = lAgendaId;
    }

    @XmlTransient
    public Long getPolicyId() {
        return m_lPolicyId;
    }

    /**
     * Set policy id
     * @param lPolicyId the policy id
     */
    public void setPolicyId( Long lPolicyId) {
        m_lPolicyId = lPolicyId;
    }

    @XmlTransient
    public String getDamages() {
        return m_sDamages;
    }

    /**
     * Set damages
     * @param sDamages the damages
     */
    public void setDamages( String sDamages ) {
        m_sDamages = sDamages;
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();

        Optional
            .ofNullable( m_lID )
            .ifPresent( id -> builder.append("ID: ").append( id ).append( "\n") );

        Optional
            .ofNullable( m_lAgendaId )
            .ifPresent( login ->
                builder.append("AGENDA ID: ").append( m_lAgendaId ).append( "\n") );

        Optional
            .ofNullable( m_lPolicyId )
            .ifPresent( description ->
                builder.append("POLICY: ").append( m_lPolicyId ).append( "\n") );

        Optional
            .ofNullable( m_sDamages )
            .ifPresent( description ->
                builder.append("DAMAGES: ").append( m_sDamages ).append( "\n") );

        return builder.toString();
    }
}
