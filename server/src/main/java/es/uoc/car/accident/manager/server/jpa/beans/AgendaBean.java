package es.uoc.car.accident.manager.server.jpa.beans;


import es.uoc.car.accident.manager.server.jpa.beans.interfaces.IObjectBean;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

@Entity
@Cacheable(false)
@Table(name = "AGENDA")
public class AgendaBean implements Serializable, IObjectBean
{
    /** ID of the event in agenda table */
    @Id
    @Column(name="ID")
    @XmlElement(name="id")
    private Long m_lID;

    /** short description of the event in agenda table */
    @Column(name="SHORT_DESCRIPTION")
    @XmlElement(name="short_description")
    private String m_shortDescription;

    /** long description of the event in agenda table */
    @Column(name="LONG_DESCRIPTION")
    @XmlElement(name="long_description")
    private String m_sLongDescription;

    /** street of the event in agenda table */
    @Column(name="STREET")
    @XmlElement(name="street")
    private String m_sStreet;

    /** city of the event in agenda table */
    @Column(name="CITY")
    @XmlElement(name="city")
    private String m_sCity;

    /** country of the event in agenda table */
    @Column(name="COUNTRY")
    @XmlElement(name="country")
    private String m_sCountry;

    /** latitude of the event in agenda table */
    @Column(name="LATITUDE")
    @XmlElement(name="latitude")
    private Double m_dLatitude;

    /** longitude of the event in agenda table */
    @Column(name="LONGITUDE")
    @XmlElement(name="longitude")
    private Double m_dLongitude;

    /** type of the event in agenda table */
    @Column(name="TYPE")
    @XmlElement(name="agenda_type")
    private Integer m_iType;

    /** subtype of the event in agenda table */
    @Column(name="SUBTYPE")
    @XmlElement(name="agenda_subtype")
    private Integer m_iSubType;

    /** status of the event in agenda table */
    @Column(name="STATUS")
    @XmlElement(name="status")
    private Integer m_iStatus;

    /** the id of the parent of the event in agenda table */
    @Column(name="ID_PARENT")
    @XmlElement(name="parent_id")
    private Long m_lParentId;

    /** start date of the parent of the event in agenda table */
    @Column(name="START_DATE")
    @XmlElement(name="start_date")
    private Date m_startDate;

    /** update date of the parent of the event in agenda table */
    @Column(name="UPDATE_DATE")
    @XmlTransient
    private Date m_updateDate;

    /** end date of the parent of the event in agenda table */
    @Column(name="END_DATE")
    @XmlElement(name="end_date")
    private Date m_endDate;

    /** The id policy of the customer who create the car accident incident */
    @Column(name="ID_POLICY")
    @XmlElement(name="policy_id")
    private Long m_lPolicyId;

    /**
     * Enum used to fill the m_iType element
     */
    public enum AgendaItemType {
        CAR_ACCIDENT,
        TASK,
        EVENT;

        /**
         * Get agenda item type from ordinal
         * @param iType the agenda type
         * @return the agenda type enum item
         */
        public static AgendaItemType getType(int iType) {
            return Arrays.asList( values())
                .stream()
                .sequential()
                .filter(  item -> item.ordinal() == iType )
                .findAny()
                .orElse(null);
        }
    }

    /**
     * The enum for agenda item subtype, really they are the task or event type
     * It is necessary the id because in the future possible it will be necessary to
     * add new tasks or events and the id of the previous one can't change, because
     * its value is stored in records of database
     */
    public enum AgendaItemSubType {
        CAR_DRIVING_TASK(0),
        TOW_TRUCK_DRIVING_TASK(1),
        DAMAGE_ASSESSMENT_TASK(2),
        CAR_REPAIRING_TASK(3),
        REHABILITATION_TASK(4),
        MEDICAL_TASK(5),
        LEGAL_TASK(6),
        CAR_DRIVING_EVENT(100),
        TOW_TRUCK_EVENT(101),
        DAMAGE_ASSESSMENT_EVENT(102),
        CAR_REPAIRING_EVENT(103),
        REHABILITATION_EVENT(104),
        MEDICAL_EVENT(105),
        LEGAL_EVENT(106);


        /**
         * Constructor
         * @param iId the id of the subtype ( task or event type )
         */
        AgendaItemSubType( Integer iId) {
            m_iId = iId;
        }

        /** The subtype id */
        protected Integer m_iId;

        /*** Get the subtype id */
        public Integer id()
        {
            return m_iId;
        }
    }
    /**
     * Constructor without any parameter
     */
    public AgendaBean() {
    }

    @Override
    @XmlTransient
    public Long getId() {
        return m_lID;
    }


    public void setId(Long Id) {
        this.m_lID = Id;
    }

    @XmlTransient
    public String getShortDescription() {
        return m_shortDescription;
    }

    public void setShortDescription(String ShortDescription) {
        this.m_shortDescription = ShortDescription;
    }

    @XmlTransient
    public String getLongDescription() {
        return m_sLongDescription;
    }

    public void setLongDescription(String LongDescription) {
        this.m_sLongDescription = LongDescription;
    }

    @XmlTransient
    public String getStreet() {
        return m_sStreet;
    }

    public void setStreet(String Street) {
        this.m_sStreet = Street;
    }

    @XmlTransient
    public String getCity() {
        return m_sCity;
    }

    public void setCity(String City) {
        this.m_sCity = City;
    }

    @XmlTransient
    public String getCountry() {
        return m_sCountry;
    }

    public void setCountry(String Country) {
        this.m_sCountry = Country;
    }

    @XmlTransient
    public Double getLatitude() {
        return m_dLatitude;
    }

    public void setLatitude(Double Latitude) {
        this.m_dLatitude = Latitude;
    }

    @XmlTransient
    public Double getLongitude() {
        return m_dLongitude;
    }

    public void setLongitude(Double Longitude) {
        this.m_dLongitude = Longitude;
    }

    @XmlTransient
    public Integer getType() {
        return m_iType;
    }

    public void setType(Integer Type) {
        this.m_iType = Type;
    }

    @XmlTransient
    public Integer getSubtype() {
        return m_iSubType;
    }

    public void setSubtype(Integer Subtype) {
        this.m_iSubType = Subtype;
    }

    @XmlTransient
    public Integer getStatus() {
        return m_iStatus;
    }

    public void setStatus(Integer Statue) {
        this.m_iStatus = Statue;
    }

    @XmlTransient
    public Long getParentId() {
        return m_lParentId;
    }

    public void setParentId(Long m_lParentId) {
        this.m_lParentId = m_lParentId;
    }

    @XmlTransient
    public Date getStartDate() {
        return m_startDate;
    }

    public void setStartDate(Date startDate) {
        this.m_startDate = startDate;
    }

    @XmlTransient
    public Date getUpdateDate() {
        return m_updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.m_updateDate = updateDate;
    }

    @XmlTransient
    public Date getEndDate() {
        return m_endDate;
    }

    public void setEndDate(Date endDate) {
        this.m_endDate = endDate;
    }

    @XmlTransient
    public Long getPolicyId() { return m_lPolicyId; }

    public void setPolicyId( Long lPolicyId ) { m_lPolicyId = lPolicyId; }

    /**
     * Copy parameters from an agenda bean
     * @param agendaBean the agenda bean containing the parameters to be copied
     * @return the new agenda bean
     */
    public AgendaBean copy( AgendaBean agendaBean ) {
        this.m_lID = agendaBean.m_lID;
        this.m_shortDescription = agendaBean.m_shortDescription;
        this.m_sLongDescription = agendaBean.m_sLongDescription;
        this.m_sStreet = agendaBean.m_sStreet;
        this.m_sCity = agendaBean.m_sCity;
        this.m_sCountry = agendaBean.m_sCountry;
        this.m_dLatitude = agendaBean.m_dLatitude;
        this.m_dLongitude = agendaBean.m_dLongitude;
        this.m_iType = agendaBean.m_iType;
        this.m_iSubType = agendaBean.m_iSubType;
        this.m_iStatus = agendaBean.m_iStatus;
        this.m_lParentId = agendaBean.m_lParentId;
        this.m_startDate = agendaBean.m_startDate;
        this.m_endDate = agendaBean.m_endDate;
        this.m_updateDate = agendaBean.m_updateDate;
        this.m_lPolicyId = agendaBean.m_lPolicyId;

        return this;
    }

    /**
     * Compare if this agenda bean is equal to the agenda bean parameter
     * @param agendaBean the agenda bean to be compared
     * @return true or false depending on the item is equal or not
     */
    public boolean areEquals( AgendaBean agendaBean ) {
        return Optional
            .ofNullable(agendaBean)
            .map(AgendaBean::toString)
            .filter(agenda -> agenda.equalsIgnoreCase(toString() ) )
            .isPresent();
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();

        Optional
                .ofNullable( m_lID )
                .ifPresent( id -> builder.append("ID: ").append( id ).append( "\n") );

        Optional
            .ofNullable( m_lParentId )
            .ifPresent( id -> builder.append("PARENT_ID: ").append( id ).append( "\n") );

        Optional
                .ofNullable( m_shortDescription )
                .ifPresent( description ->
                        builder.append("SHORT DESCRIPTION: ").append( description ).append( "\n") );

        Optional
            .ofNullable( m_sLongDescription )
            .ifPresent( description ->
                builder.append("LONG DESCRIPTION: ").append( description ).append( "\n") );


        Optional
                .ofNullable( m_iType )
                .ifPresent( type -> builder.append("TYPE: ").append( type ).append( "\n") );

        Optional
                .ofNullable( m_iSubType )
                .ifPresent( subtype -> builder.append("SUBTYPE: ").append( subtype ).append( "\n") );

        Optional
                .ofNullable( m_iStatus )
                .ifPresent( status -> builder.append("STATUS: ").append( status ).append( "\n") );

        Optional
                .ofNullable( m_dLongitude )
                .ifPresent( longitude -> builder.append("LONGITUDE: ").append( longitude ).append( "\n") );

        Optional
                .ofNullable( m_dLatitude )
                .ifPresent( latitude -> builder.append("LATITUDE: ").append( latitude ).append( "\n") );

        Optional
            .ofNullable( m_sCity )
            .ifPresent( city ->
                builder.append("CITY: ").append( city ).append( "\n") );

        Optional
            .ofNullable( m_sStreet )
            .ifPresent( street ->
                builder.append("STREET: ").append( street ).append( "\n") );

        Optional
            .ofNullable( m_sCountry )
            .ifPresent( country ->
                builder.append("COUNTRY: ").append( country ).append( "\n") );

        Optional
            .ofNullable( m_startDate )
            .ifPresent( startDate ->
                builder.append("START DATE: ").append( startDate ).append( "\n") );

        Optional
            .ofNullable( m_updateDate )
            .ifPresent( updateDate ->
                builder.append("UPDATE DATE: ").append( updateDate ).append( "\n") );

        Optional
            .ofNullable( m_endDate )
            .ifPresent( endDate ->
                builder.append("START DATE: ").append( endDate ).append( "\n") );

        Optional
            .ofNullable( m_lPolicyId )
            .ifPresent( policyId ->
                builder.append("POLICY ID: ").append( policyId ).append( "\n") );

        return builder.toString();
    }
}
