package es.uoc.car.accident.manager.server.servlets;

import es.uoc.car.accident.manager.server.jpa.beans.UserBean;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.Optional;

import static es.uoc.car.accident.manager.server.constants.CarAccidentServiceConstants.USER_SESSION_ATTRIBUTE;
import static es.uoc.car.accident.manager.server.servlets.AuthenticationServlet.sendUserBean;


public class LogoutServlet extends HttpServlet {

    public void doGet( HttpServletRequest request, HttpServletResponse response )
        throws ServletException, IOException {

        UserBean userBean = Optional
            .ofNullable(request)
            .map(r -> r.getSession(false))
            .map(session -> session.getAttribute(USER_SESSION_ATTRIBUTE))
            .map(UserBean.class::cast)
            .orElse(null);

        /** If no user bean throw servlet exception */
        if( userBean == null ) {
            throw new ServletException("No user in session");
        }

        /** Send user bean */
        try {
            sendUserBean(response, userBean);
        } catch (IOException | JAXBException e) {
            throw new ServletException(e);
        }

        Optional
            .ofNullable( request)
            .map(r -> r.getSession(false) )
            .ifPresent(httpSession -> httpSession.invalidate());

        LoggerFactory.getLogger( AuthenticationServlet.class ).info( "USER LOGOUT\n{}" , userBean );
    }
}
