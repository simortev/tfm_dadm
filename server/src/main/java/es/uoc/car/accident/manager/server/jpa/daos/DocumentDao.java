package es.uoc.car.accident.manager.server.jpa.daos;

import es.uoc.car.accident.manager.server.jpa.beans.DocumentBean;
import es.uoc.car.accident.manager.server.json.services.RecursiveTransaction;
import es.uoc.car.accident.manager.server.json.services.RestServiceException;
import es.uoc.car.accident.manager.server.log.ServiceLogger;
import es.uoc.car.accident.manager.server.thread.local.manager.ThreadLocalManager;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.eclipse.persistence.jaxb.MarshallerProperties;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.*;

import static es.uoc.car.accident.manager.server.constants.CarAccidentServiceConstants.*;
import static es.uoc.car.accident.manager.server.log.ServiceLogger.BeanOperation.UPDATE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.Files.copy;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.*;

/**
 * This class is used to add, update, get or remove documents
 */
public class DocumentDao  {

    /** Provide id for agenda events */
    private static AutoIncrementProvider m_autoIncrementeProvider = new AutoIncrementProvider();

    /** Directory where to store documents */
    private static String m_sDocumentDirectory;

    /**
     * Constructor, this class is not created
     */
    private DocumentDao() {

    }

    /**
     * Set document directory
     * @param sDocumentDirectory the document directory
     */
    public static void setDocumentDirectory( String sDocumentDirectory ) {
        m_sDocumentDirectory = sDocumentDirectory;
    }

    /**
     * Add a document
     * @param documentJson the document json string
     * @param documentStream the document stream
     * @return the document bean
     * @throws RestServiceException the exception thrown in case of error
     */
    public static DocumentBean addDocument( String documentJson, InputStream documentStream )
    throws Throwable  {

        DocumentBean document;

        if( documentJson == null ) {
            throw new RestServiceException( "Error in request, no json ", BAD_REQUEST.getStatusCode());
        }

        if( documentStream == null ) {
            throw new RestServiceException( "Error in request, no file ",  BAD_REQUEST.getStatusCode());
        }

        Map<String, Object> properties = new HashMap<String, Object>(1);
        properties.put(MarshallerProperties.JSON_WRAPPER_AS_ARRAY_NAME, false);

        try {
            InputStream stream = new ByteArrayInputStream( documentJson.getBytes( UTF_8 ) );
            StreamSource source = new StreamSource(stream);

            Map<String, Object> jaxbProperties = new HashMap<>(2);
            jaxbProperties.put(JAXBContextProperties.MEDIA_TYPE, APPLICATION_JSON);
            jaxbProperties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, false);

            JAXBContext jc = JAXBContext.newInstance(new Class[]{DocumentBean.class}, jaxbProperties);
            Unmarshaller unmarshaller = jc.createUnmarshaller();

            document = unmarshaller.unmarshal( source , DocumentBean.class ).getValue();
        }
        catch ( Exception e ) {
            e.printStackTrace();
            throw new RestServiceException( "Error in request json " + e.getMessage(),
                BAD_REQUEST.getStatusCode());
        }

        Long lAgendaId = Optional
        .ofNullable( document )
        .map( DocumentBean::getAgendaId )
        .orElse( null );

        String sFileName = Optional
            .ofNullable( document )
            .map( DocumentBean::getFileName )
            .orElse( null );

        Long lDocumentId = m_autoIncrementeProvider.getId();

        if( lAgendaId == null ) {
            throw new RestServiceException( "Error in request no agenda id ",  BAD_REQUEST.getStatusCode());
        }

        if( sFileName == null ) {
            throw new RestServiceException( "Error in request no file name ",  BAD_REQUEST.getStatusCode());
        }

        /** Verify if user can add document in this event */
        if ( GrantsDao.hasAuthorizationOnAgendaItem( lAgendaId ) == false) {
            new RestServiceException("User has no authorization", USER_NO_AUTHORIZED );
        }

        DocumentBean documentBean =
            DocumentBean.createDocumentBean( lDocumentId, lAgendaId, sFileName , document.getDescription() );

        /** final path for the document */
        String sPath = m_sDocumentDirectory + File.separator + lDocumentId.toString();

        try {
            copy( documentStream, new File(sPath).toPath(), REPLACE_EXISTING);

        }
        catch ( Exception e ) {
            throw new RestServiceException( "Error copy file" + e.getMessage(),
                INTERNAL_SERVER_ERROR.getStatusCode());
        }

        /** Add document info into database */
        RecursiveTransaction transaction = ThreadLocalManager.getRecursiveTransaction();
        transaction.persist( documentBean );

        AgendaDao.setUpdateTime( documentBean.getAgendaId() );

        return documentBean;
    }

    /**
     * Add a document
     * @param document the document to be updated
     * @return the document bean
     * @throws RestServiceException the exception thrown in case of error
     */
    public static DocumentBean updateDocument( DocumentBean document )
        throws Throwable  {

        /** Verify if user can update document in this event */
        if ( GrantsDao.hasAuthorizationOnAgendaItem( document.getAgendaId() ) == false) {
            new RestServiceException("User has no authorization", USER_NO_AUTHORIZED );
        }

        ThreadLocalManager.getRecursiveTransaction().merge(document);

        AgendaDao.setUpdateTime( document.getAgendaId() );

        ServiceLogger.info( DamageDao.class , UPDATE , document );

        return document;
    }

        /**
         * Get documents of agenda id
         * @param lAgendaId the agenda id
         * @return the list of documents
         * @throws RestServiceException
         */
    public static List<DocumentBean> getDocuments(Long lAgendaId )
        throws RestServiceException {

        if( lAgendaId == null ) {
            throw new RestServiceException( "Error in request no agenda id ",  BAD_REQUEST.getStatusCode());
        }

        /** Verify if user can add document in this event */
        if ( GrantsDao.hasAuthorizationOnAgendaItem( lAgendaId ) == false) {
            new RestServiceException("User has no authorization", USER_NO_AUTHORIZED );
        }

       return ObjectDao.getObjectList(
            SELECT_DOCUMENT_FROM_DOCUMENTS_TABLE,
            DOCUMENT_TABLE_AGENDA_ID_CONDITION,
            SQL_AGENDA_ID_PARAMETER,
            lAgendaId);
    }

    /**
     * Get document for its id
     * @param lDocumentId the document id
     * @return the response
     */
    public static Response getDocument(Long lDocumentId ) {

        DocumentBean document = ThreadLocalManager.getRecursiveTransaction()
            .find( DocumentBean.class , lDocumentId );

        Long lAgendaId = Optional
            .ofNullable(document)
            .map(DocumentBean::getAgendaId)
            .orElse(null);

        /** Verify if user can add document in this event */
        if (GrantsDao.hasAuthorizationOnAgendaItem(lAgendaId) == false) {
            new RestServiceException("User has no authorization", USER_NO_AUTHORIZED);
        }

        String sFileName = Optional
            .ofNullable(document)
            .map(DocumentBean::getFileName)
            .orElse(null);

        /** final path for the document */
        String sPath = m_sDocumentDirectory + File.separator + lDocumentId.toString();

        return Response
            .ok( new File( sPath ) , MediaType.APPLICATION_OCTET_STREAM)
            .header(CONTENT_DISPOSITION_HEADER, String.format(ATTACHMENT_FILENAME_FORMAT , sFileName ) )
            .build();
    }

    /**
     * remove document
     * @param lDocumentId the document id to be removed
     * @return the document bean removed or exception in case of error
     */
    public static DocumentBean removeDocument(Long lDocumentId )
        throws Throwable{

        RecursiveTransaction transaction = ThreadLocalManager.getRecursiveTransaction();

        DocumentBean document = transaction.find( DocumentBean.class , lDocumentId );

        Long lAgendaId = Optional
            .ofNullable(document)
            .map(DocumentBean::getAgendaId)
            .orElse(null);

        /** Verify if user can add document in this event */
        if( GrantsDao.hasAuthorizationOnAgendaItem(lAgendaId) == false) {
            throw new RestServiceException("User has no authorization", USER_NO_AUTHORIZED);
        }

        transaction.remove( document );

        /** final path for the document */
        String sPath = m_sDocumentDirectory + File.separator + lDocumentId.toString();

        File file = new File( sPath );
        file.delete();

        AgendaDao.setUpdateTime( document.getAgendaId() );

        return document;
    }

    /**
     * remove document
     * @param lAgendaId the document id to be removed
     * @return the document bean removed or exception in case of error
     */
    public static void removeDocuments( Long lAgendaId )
        throws Throwable {

        RecursiveTransaction transaction = ThreadLocalManager.getRecursiveTransaction();
        transaction.begin();

        List<DocumentBean> documents = Optional
            .ofNullable(getDocuments(lAgendaId))
            .orElseGet(ArrayList::new);

        try {
            for (DocumentBean document : documents) {
                removeDocument(document.getId());
            }
            transaction.commit();
        }
        catch (Throwable e ){
            transaction.rollback();
            throw  e;
        }
    }
}
