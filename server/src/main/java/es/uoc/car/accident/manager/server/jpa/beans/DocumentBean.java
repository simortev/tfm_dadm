package es.uoc.car.accident.manager.server.jpa.beans;

import es.uoc.car.accident.manager.server.jpa.beans.interfaces.IObjectBean;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

@Entity
@Cacheable(false)
@Table(name = "DOCUMENTS")
public class DocumentBean implements IObjectBean,Serializable  {

    /** ID of the event in agenda table */
    @Id
    @Column(name="ID")
    @XmlElement(name="id")
    private Long m_lID;

    /** Agenda id */
    @Column(name="ID_AGENDA")
    @XmlElement(name="agenda_id")
    private Long m_lAgendaId;

    /** File name */
    @Column(name="NAME")
    @XmlElement(name="file_name")
    private String m_sFileName;

    /** Short description */
    @Column(name="SHORT_DESCRIPTION")
    @XmlElement(name="short_description")
    private String m_sDescription;

    /**
     * Constructor
     */
    public DocumentBean() {
    }

    /**
     * Get Id
     * @return the grant id
     */
    @Override
    @XmlTransient
    public Long getId() {
        return m_lID;
    }


    /**
     * Get agenda id
     * @return the agenda id
     */
    @XmlTransient
    public Long getAgendaId() {
        return m_lAgendaId;
    }

    /**
     * Get file name
     * @return the file name
     */
    @XmlTransient
    public String getFileName() {
        return m_sFileName;
    }

    /**
     * Get description
     * @return get description
     */
    @XmlTransient
    public String getDescription() {
        return m_sDescription;
    }


    /**
     * Create a document bean bean from its properties
     * @param ID the grant id
     * @param agendaId the agenda id
     * @param sName the file name
     * @param sDescription the document short description
     * @return the grant bean
     */
    public static DocumentBean createDocumentBean(Long ID , Long agendaId , String sName, String sDescription ) {

        DocumentBean grantBean = new DocumentBean();
        grantBean.m_lAgendaId = agendaId;
        grantBean.m_lID = ID;
        grantBean.m_sFileName = sName;
        grantBean.m_sDescription = sDescription;

        return grantBean;
    }
}
