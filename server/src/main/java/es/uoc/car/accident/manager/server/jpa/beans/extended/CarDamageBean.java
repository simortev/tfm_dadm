package es.uoc.car.accident.manager.server.jpa.beans.extended;

import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import es.uoc.car.accident.manager.server.jpa.beans.DamageBean;
import es.uoc.car.accident.manager.server.jpa.beans.interfaces.IObjectBean;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Optional;

/**
 * The damage bean with user and car policy
 */
public class CarDamageBean extends CarInfoBean implements IObjectBean {

    /** The car damages */
    @XmlElement(name="damages")
    private DamageBean m_damages;

    /**
     * Constructor
     */
    public CarDamageBean() {

    }

    /**
     * Constructor
     * @param damages the damage of this car
     * @param carInfoBean the car info bean
     */
    public CarDamageBean(DamageBean damages, CarInfoBean carInfoBean ) {
        super(carInfoBean);
        m_damages = damages;
    }

    @XmlTransient
    public DamageBean getDamages() {
        return m_damages;
    }

    @Override
    public Long getId() {
        return Optional.ofNullable(m_damages).map(DamageBean::getId).orElse(null);
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();

        builder.append( super.toString() ).append( "\n");

        Optional
            .ofNullable( m_damages )
            .ifPresent( carInfoBean -> builder.append( m_damages ).append( "\n") );

        return builder.toString();
    }

}
