package es.uoc.car.accident.manager.server.jpa.daos;

import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean.AgendaItemSubType;
import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean.AgendaItemType;
import es.uoc.car.accident.manager.server.jpa.beans.GrantBean;
import es.uoc.car.accident.manager.server.jpa.beans.PolicyBean;
import es.uoc.car.accident.manager.server.jpa.beans.UserBean;
import es.uoc.car.accident.manager.server.jpa.beans.UserBean.Role;
import es.uoc.car.accident.manager.server.thread.local.manager.ThreadLocalManager;
import org.slf4j.LoggerFactory;

import javax.persistence.Query;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static es.uoc.car.accident.manager.server.constants.CarAccidentServiceConstants.*;
import static es.uoc.car.accident.manager.server.jpa.beans.AgendaBean.AgendaItemSubType.*;
import static es.uoc.car.accident.manager.server.jpa.beans.UserBean.Role.*;

/**
 * Data access objet to get grants permissions
 *
 * Usually this dao is used from the agenda dao and the entity manager is already created
 */
public class GrantsDao  {
    /** Provide id for grants events */
    private static AutoIncrementProvider m_autoIncrementeProvider = new AutoIncrementProvider();

    /**
     * Get user session data
     * @return the user session data
     */
    public static  UserBean getUserSession() {
        return ThreadLocalManager.getUserBean();
    }

    /**
     * Relation between user roles and tasks
     */
    public enum Grants {

        GRANT_CAR_DRIVING_TASK( CAR_DRIVER , CAR_DRIVING_TASK  ),
        GRANT_TOW_TRUCK_DRIVING_TASK( TOW_TRUCK_DRIVER , TOW_TRUCK_DRIVING_TASK  ),
        GRANT_DAMAGE_ASSESSMENT_TASK( LOSS_ADJUSTER , DAMAGE_ASSESSMENT_TASK),
        GRANT_CAR_REPAIRING_TASK( CAR_MECHANIC , CAR_REPAIRING_TASK  ),
        GRANT_REHABILITATION_TASK( PHYSIOTHERAPIST , REHABILITATION_TASK  ),
        GRANT_MEDICAL_TASK(MEDICAL_PRACTITIONER, MEDICAL_TASK  ),
        GRANT_LEGAL_TASK( LAWYER , LEGAL_TASK );

        /** The role of user */
        private Role m_role;
        /** The agenda task */
        private AgendaItemSubType m_task;

        /**
         * Constructor
         * @param role the user role
         * @param task the agenda task
         */
        Grants(Role role, AgendaItemSubType task) {
            this.m_role = role;
            this.m_task = task;
        }

        /**
         * Get role
         * @return the role
         */
        public Role role() {
            return m_role;
        }

        /**
         * Get task
         * @return the task
         */
        public AgendaItemSubType task() {
            return m_task;
        }

        /**
         * Return role from task id
         * @param iTask the task id
         * @return return the role
         */
        public static Role getGrantedRole(int iTask ) {
            return Arrays.asList( values() )
                .stream()
                .sequential()
                .filter( grant -> grant.task().id() == iTask )
                .findAny()
                .map(Grants::role )
                .orElse(null);
        }
    }


    /**
     * Verify condition if user can create agenda item, depending on its role and if user has permission on
     * agenda item parent
     *
     * car accident customers or insurance staff
     *
     * tasks only insurance staff ( they have to authorize the intervention of the professional staff )
     *
     * events only created by insurance or professional staff ( no customers )
     *
     * @param agendaBean the agenda to verify permissions
     * @return true if if has permission false in other case
     */
    public static boolean hasAuthorizationOnCreateAgendaItem(AgendaBean agendaBean ) {

        Predicate<Integer> roleCondition;

        switch(AgendaItemType.getType( agendaBean.getType() )){
            case CAR_ACCIDENT:{
                roleCondition = role -> role == CUSTOMER.id() || role == INSURANCE_STAFF.id();
                break;
            }
            case TASK:{
                roleCondition = role -> role == INSURANCE_STAFF.id();
                break;
            }
            case EVENT:{
                roleCondition = role ->  role != CUSTOMER.id();
                break;
            }
            default:{
                roleCondition = role -> false;
                break;
            }
        }

        UserBean userBean = getUserSession();

        boolean bAuthorized = Optional
            .ofNullable( userBean )
            .map( UserBean::getRole )
            .filter( roleCondition::test )
            .filter( role -> hasAuthorizationOnParentAgendaItem( agendaBean ) )
            .isPresent();

        if( !bAuthorized ) {
            LoggerFactory.
                getLogger( AgendaBean.class )
                .error("USER {} NO AUTHORIZED ADDING AGENDA\n{}" , userBean.getName() , agendaBean );
        }

        return bAuthorized;
    }

    /**
     * If this role has authorization on crate this event
     * @param agendaBean the agenda bean to be created
     * @return true or false with the authorization response
     */
    private static boolean hasAuthorizationOnParentAgendaItem(AgendaBean agendaBean) {
       return Optional
           .ofNullable( agendaBean )
           .map( AgendaBean::getParentId )
           // It is a map ( no a filter ) it returns true or false if there is parent id
           .map( GrantsDao::hasAuthorizationOnAgendaItem)
           // No parent id, then it is a car accident event, no restriction to create one
           .orElse( true );
    }


    /**
     * Verify if a user has permissions to access this agenda event
     * @param agendaBean the agenda to verify permissions
     * @return true if if has permission false in other case
     */
    public static boolean hasAuthorizationOnAgendaItem(AgendaBean agendaBean ) {

        /** Get id to verify permissions */
        return Optional
            .ofNullable(agendaBean)
            .map(AgendaBean::getId)
            .filter( GrantsDao::hasAuthorizationOnAgendaItem)
            .isPresent();
    }

    /**
     * Verify if a user has permissions to access this agenda event
     * @param lAgendaId the event agenda id
     * @return true if if has permission false in other case
     */
    public static boolean hasAuthorizationOnAgendaItem(Long lAgendaId ){

        /** Get the user id for granting agenda */
        UserBean userBean = getUserSession();

        Query query = ThreadLocalManager.getRecursiveTransaction()
            .createQuery( SELECT_GRANT_FROM_GRANTS_TABLE
            +  GRANT_TABLE_AGENDA_ID_CONDITION
            +  GRANT_TABLE_USER_ID_CONDITION );

        query.setParameter( SQL_USER_ID_PARAMETER , userBean.getId() );
        query.setParameter( SQL_AGENDA_ID_PARAMETER , lAgendaId );

        /** If exist an item then , the user has grants*/
        boolean bAuthorized = Optional
            .ofNullable( query )
            .map( Query::getResultList )
            .filter( list -> list.isEmpty() == false )
            .isPresent();

        if( !bAuthorized ) {
            LoggerFactory.
                getLogger( AgendaBean.class )
                .error("USER {} NO AUTHORIZED ON ITEM AGENDA {}" , userBean.getName() , lAgendaId );
        }

        return bAuthorized;
    }


    /**
     * Get a user with this role
     * @param role the role of the user
     * @return the user id
     */
    private static Long getRandomUserWithRole( Role role ) {

        List<UserBean> hUsers = ObjectDao.getObjectList(
            SELECT_USER_FROM_USERS_TABLE,
            USERS_TABLE_USER_ROLE_CONDITION,
            SQL_USER_ROLE_ID_PARAMETER,
            role.id());

        if( hUsers.isEmpty() ){
            LoggerFactory.getLogger(GrantsDao.class).error( "NO USER WITH ROLE {}" , role );
            return null;
        }

        int iIndex = (int ) Math.random() * hUsers.size();

        return hUsers.get( iIndex > 0 && iIndex < hUsers.size() ? iIndex : 0 ).getId();
    }

    /**
     * Add permission to access this agenda event to this list of users
     *
     *   all the users with authorization on parent are going to be granted
     *
     *   car accident -> parent users + user + an insurance worker ( random )
     *   task -> parent users + professional worker ( random )
     *   event -> parent users
     *
     * @param agendaBean the agenda to be added
     */
    public static void addGrants(AgendaBean agendaBean )
    throws Throwable{
        List<GrantBean> hGrants = new ArrayList<>();

        switch(AgendaItemType.getType( agendaBean.getType() )){
            case CAR_ACCIDENT:{
                hGrants.addAll( getCarAccidentGrants(agendaBean) );
                break;
            }
            case TASK:{
                hGrants.addAll( getTaskGrants(agendaBean) );
                break;
            }
            case EVENT:{
                hGrants.addAll( getEventGrants(agendaBean) );
                break;
            }
            default:{
                break;
            }
        }

        /** Add grants fro agenda bean */
       ThreadLocalManager.getRecursiveTransaction().persist(hGrants);

       LoggerFactory.getLogger(AgendaBean.class).info( "ADD GRANTS: {}" , hGrants );
    }

    /**
     * Authorization to customer and one insurance staff
     * @param agendaBean the agenda id
     * @return the grants of consumer and insurance staff who is going to manage the car accident procedure
     */
    private static List<GrantBean>  getCarAccidentGrants( AgendaBean agendaBean ) {
        List<GrantBean> hGrants = new ArrayList<>();

        UserBean session = ThreadLocalManager.getUserBean();
        List<Long> hUsersId = new ArrayList<>();

        Long lCarAccidentId = Optional
            .ofNullable(agendaBean)
            .map(AgendaBean::getId)
            .orElse(null);

        /** Grant user who is creating the car accident, customer or insurance staff */
        Optional
            .ofNullable(session)
            .map(UserBean::getId)
            .ifPresent(hUsersId::add);

         /** If user is the customer then select the insurance staff from random */
         if( session.getRole() == Role.CUSTOMER.id() ) {
             Optional
                 .ofNullable(INSURANCE_STAFF)
                 .map(GrantsDao::getRandomUserWithRole)
                 .filter(id -> hUsersId.contains(id) == false)
                 .ifPresent(hUsersId::add);
         }
         /** If user is insurance staff then add the customer from policy id of car accident */
         else{
             Optional
                 .ofNullable( agendaBean )
                 /** Get policy of car accident */
                 .map(AgendaBean::getPolicyId)
                 .map(PolicyDao::getPolicy)
                 /** Get user id of policy */
                 .map( PolicyBean::getUserId )
                 .ifPresent( hUsersId::add );
         }


        /** Add grants for can accident */
        hUsersId
            .stream()
            .sequential()
            .map( userId -> GrantBean.createGrantBean( m_autoIncrementeProvider.getId(), userId, lCarAccidentId) )
            .forEach( hGrants::add );

        return hGrants;
    }

    /**
     * Get task grants
     *
     * Grants for the insurance worker ( who is creating this task ) , one professional staff
     * get at random in this release and user who created the car accident
     *
     * Grants for task id and for car accident id
     *
     * @param agendaBean the agenda bean
     * @return the list of grants, the worker in task and the worker in car accident
     */
    private static List<GrantBean> getTaskGrants(AgendaBean agendaBean) {
        List<GrantBean> hGrants = new ArrayList<>();
        List<Long> hUsersId = new ArrayList<>();

        Long lTaskId = Optional
            .ofNullable(agendaBean)
            .map(AgendaBean::getId)
            .orElse(null);

        Long lCarAccidentId = Optional
            .ofNullable(agendaBean)
            .map(AgendaBean::getParentId)
            .orElse(null);

        if (lTaskId == null || lCarAccidentId == null) {
            LoggerFactory.getLogger(GrantsDao.class).error("Bad task id or parent id\n{}",agendaBean);
            return new ArrayList<>();
        }

        // Authorization to a random professional with the role granted for this task
        Long lStaffUserId = Optional
                .ofNullable( agendaBean )
                /** Get the user role to do this type of task */
                .map(AgendaBean::getSubtype)
                .map(Grants::getGrantedRole)
                /** Get randomly a user from database with this role */
                .map(GrantsDao::getRandomUserWithRole)
                .orElse( null );

       Optional
             .ofNullable( lStaffUserId)
              .ifPresent( hUsersId::add );

        // The insurance staff who is creating this task ( the user session )
         Optional
            .ofNullable( getUserSession() )
            .map( UserBean::getId )
             .filter( id -> hUsersId.contains(id) == false )
            .ifPresent( hUsersId::add );

        // The customer who underwent the car accident
        Optional
            /** Ger car accident bean from data base */
            .ofNullable( lCarAccidentId )
            .map(AgendaDao::getAgendaById)
            /** Get the policy Id of the car accident, it contains the user who had the accident */
            .map(AgendaBean::getPolicyId)
            /** Get the policy data from the policy id */
            .map(PolicyDao::getPolicy)
            /** Get user id from the policy */
            .map(PolicyBean::getUserId)
            .filter( id -> hUsersId.contains(id) == false )
            .ifPresent( hUsersId::add );

        /** Add grants for task id */
        hUsersId
            .stream()
            .sequential()
            .map( userId -> GrantBean.createGrantBean( m_autoIncrementeProvider.getId(), userId, lTaskId ) )
            .forEach( hGrants::add );

        /** Add grants for staff on car accident */

       hGrants.add(GrantBean.createGrantBean( m_autoIncrementeProvider.getId(), lStaffUserId, lCarAccidentId  ) );

        return  hGrants;
    }

    /**
     * Get event grants, the event is granted for all the users who has the parent task granted
     * @param agendaBean the agenda bean
     * @return list of grants
     */
    private static List<GrantBean> getEventGrants(AgendaBean agendaBean) {
        
         List<GrantBean> hParentGrants = ObjectDao.getObjectList(
            SELECT_GRANT_FROM_GRANTS_TABLE,
            GRANT_TABLE_AGENDA_ID_CONDITION,
            SQL_AGENDA_ID_PARAMETER,
            agendaBean.getParentId());

        return Optional
            .ofNullable(hParentGrants)
            .orElseGet(ArrayList::new)
            .stream()
            .sequential()
            .map( GrantBean::getUserId )
            .map( userId -> GrantBean.createGrantBean( m_autoIncrementeProvider.getId(), userId, agendaBean.getId() ) )
            .collect( Collectors.toList());
    }

}
