package es.uoc.car.accident.manager.server.jpa.beans;


import es.uoc.car.accident.manager.server.jpa.beans.interfaces.IObjectBean;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Optional;

@Entity
@Cacheable(false)
@Table(name = "POLICIES")
public class PolicyBean implements IObjectBean,Serializable {

    @Id
    @Column(name="ID")
    @XmlElement(name="id")
    private Long m_lID;

    /** User id */
    @Column(name="ID_USER")
    @XmlElement(name="user_id")
    private Long m_lUserId;

    /** The policy number */
    @Column(name="POLICY_NUMBER")
    @XmlElement(name="policy_number")
    private String m_sPolicyNumber;

    /** The car model */
    @Column(name="CAR_MODEL")
    @XmlElement(name="car_model")
    private String m_sCarModel;

    /** The car plate number */
    @Column(name="CAR_PLATE_NUMBER")
    @XmlElement(name="car_plate_number")
    private String m_sCarPlateNumber;

    /** The car plate number */
    @Column(name="CAR_AGE")
    @XmlElement(name="car_age")
    private Integer m_iCarAge;

    /** The car plate number */
    @Column(name="COMPANY")
    @XmlElement(name="company")
    private String m_sCompany;

    public PolicyBean() {

    }

    @Override
    @XmlTransient
    public Long getId() {
        return m_lID;
    }

    @XmlTransient
    public Long getUserId() {
        return m_lUserId;
    }

    @XmlTransient
    public String getPolicyNumber() {
        return m_sPolicyNumber;
    }

    @XmlTransient
    public String getCarModel() {
        return m_sCarModel;
    }

    @XmlTransient
    public String getCarPlateNumber() {
        return m_sCarPlateNumber;
    }

    @XmlTransient
    public Integer getCarAge() {
        return m_iCarAge;
    }

    @XmlTransient
    public String getCompany() {
        return m_sCompany;
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();

        Optional
            .ofNullable( m_lID )
            .ifPresent( id -> builder.append("ID: ").append( id ).append( "\n") );
        Optional
            .ofNullable( m_lUserId )
            .ifPresent( login ->
                builder.append("USER: ").append( m_lUserId ).append( "\n") );

        Optional
            .ofNullable( m_sPolicyNumber )
            .ifPresent( login ->
                builder.append("POLICY NUMBER: ").append( m_sPolicyNumber ).append( "\n") );

        Optional
            .ofNullable( m_sCompany )
            .ifPresent( description ->
                builder.append("COMPANY: ").append( m_sCompany ).append( "\n") );

        Optional
            .ofNullable( m_sCarModel )
            .ifPresent( description ->
                builder.append("CAR MODEL: ").append( m_sCarModel ).append( "\n") );

        Optional
            .ofNullable( m_sCarPlateNumber )
            .ifPresent( role ->
                builder.append("CAR PLATE NUMBER: ").append( m_sCarPlateNumber ).append( "\n") );

        Optional
            .ofNullable( m_iCarAge )
            .ifPresent( driverLicenseNumber ->
                builder.append("CAR AGE: ").append( m_iCarAge ).append( "\n") );

        return builder.toString();
    }
}
