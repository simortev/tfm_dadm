package es.uoc.car.accident.manager.server.jpa.beans.extended;

import es.uoc.car.accident.manager.server.jpa.beans.PolicyBean;
import es.uoc.car.accident.manager.server.jpa.beans.UserBean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Optional;

/**
 * Car info bean
 */
public class CarInfoBean {

    /** the policy */
    @XmlElement(name="policy")
    private PolicyBean m_policy;

    /** the user */
    @XmlElement(name="user")
    private UserBean m_user;

    /**
     * Constructor
     */
    public CarInfoBean() {
    }

    /**
     * Constructor
     * @param policy the policy
     * @param user the user of the policy
     */
    public CarInfoBean(PolicyBean policy , UserBean user) {
        m_user = user;
        m_policy = policy;
    }


    /**
     * Constructor
     * @param carInfoBean the car info bean
     */
    public CarInfoBean( CarInfoBean carInfoBean ) {
        m_policy = carInfoBean.getPolicy();
        m_user = carInfoBean.getUser();
    }

    /**
     * Get policy
     * @return the policy
     */
    @XmlTransient
    public PolicyBean getPolicy() {
        return m_policy;
    }

    /**
     * Get user
     * @return the user
     */
    @XmlTransient
    public UserBean getUser() {
        return m_user;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        Optional
            .ofNullable( m_user )
            .ifPresent( login ->
                builder.append( m_user ).append( "\n") );

        Optional
            .ofNullable( m_policy )
            .ifPresent( login ->
                builder.append( m_policy ).append( "\n") );

        return builder.toString();
    }
}
