package es.uoc.car.accident.manager.server.jpa.daos;

import es.uoc.car.accident.manager.server.jpa.beans.*;
import es.uoc.car.accident.manager.server.jpa.beans.extended.CarDamageBean;
import es.uoc.car.accident.manager.server.json.services.RecursiveTransaction;
import es.uoc.car.accident.manager.server.json.services.RestServiceException;
import es.uoc.car.accident.manager.server.log.ServiceLogger;
import es.uoc.car.accident.manager.server.thread.local.manager.ThreadLocalManager;

import java.util.*;


import static es.uoc.car.accident.manager.server.constants.CarAccidentServiceConstants.*;
import static es.uoc.car.accident.manager.server.log.ServiceLogger.BeanOperation.*;

/**
 * Object used in a static way to get information about damages
 */
public class DamageDao {

    /** Provide id for damages */
    private static AutoIncrementProvider m_autoIncrementProvider = new AutoIncrementProvider();

    /** To avoid this class can be instantiated */
    private DamageDao() {

    }


    /**
     * Get list of damages
     * @param lDamagesById the damage id
     * @return a damage bean
     */
    public static DamageBean getDamageById(Long lDamagesById ) {

        DamageBean damageBean = ThreadLocalManager.getRecursiveTransaction()
            .find( DamageBean.class , lDamagesById );

        Long lAgendaId = Optional
            .ofNullable(damageBean)
            .map(DamageBean::getAgendaId)
            .orElse(null);

        /** Verify if user can add document in this event */
        if (GrantsDao.hasAuthorizationOnAgendaItem(lAgendaId) == false) {
            new RestServiceException("User has no authorization", USER_NO_AUTHORIZED);
        }

        ServiceLogger.info( AgendaDao.class , "GET DAMAGES BY ID " + lDamagesById );

        return damageBean;
    }

    /**
     * Get list of damages
     * @param lCarAccidentId the car accident id
     * @return a list of agenda events
     */
    public static List<CarDamageBean> getDamages(Long lCarAccidentId )
    throws RestServiceException {

        /** Uer has permissions in car accident  */
        Optional
            .ofNullable( lCarAccidentId )
            .filter( GrantsDao::hasAuthorizationOnAgendaItem)
            .orElseThrow(() ->
                new RestServiceException("User has no authorization on get damages in car accident " + lCarAccidentId , USER_NO_AUTHORIZED ) );

        List<DamageBean> hDamages = ObjectDao.getObjectList(
                    SELECT_DAMAGE_FROM_DAMAGES_TABLE,
                    DAMAGES_TABLE_AGENDA_ID_CONDITION ,
                    SQL_AGENDA_ID_PARAMETER,
                    lCarAccidentId );

        ServiceLogger.info( DamageDao.class ,
            "GET DAMAGES OF CAR ACCIDENT: " + lCarAccidentId + " NUMBER: " + hDamages.size() );

        return ObjectDao.getCarInfoBeans(hDamages, CarDamageBean::new, DamageBean::getPolicyId);
    }


    /**
     * Add damage bean
     * @param damageBean the damage bean
     * @return the damage bean
     */
    public static DamageBean addDamage( DamageBean damageBean )
        throws Throwable {

        /** Uer has permissions in agenda  */
        Optional
            .ofNullable( damageBean )
            .map(DamageBean::getAgendaId)
            .filter( GrantsDao::hasAuthorizationOnAgendaItem)
            .orElseThrow(() -> new RestServiceException("User has no authorization on create damage ", USER_NO_AUTHORIZED ) );

        /** Set random id in damage bean */
        Optional
            .ofNullable( damageBean )
            .ifPresent( bean -> bean.setId( m_autoIncrementProvider.getId() ));

        ThreadLocalManager.getRecursiveTransaction().persist( damageBean );

        AgendaDao.setUpdateTime( damageBean.getAgendaId() );

        ServiceLogger.info( DamageDao.class , ADD , damageBean  );

        return damageBean;
    }

    /**
     * Add damage bean for this policy number
     * @param damageBean the damage bean
     * @param sPolicyNumber the policy number
     * @return the damage bean
     */
    public static DamageBean addDamage( DamageBean damageBean , String sPolicyNumber )
        throws Throwable {

        Long lPolicyId = Optional
            .ofNullable(sPolicyNumber )
            .map( PolicyDao::getPolicyByPolicyNumber)
            .map(PolicyBean::getId)
            .orElse(null);

        if( lPolicyId == null ) {
            throw new RestServiceException("Bad policy number " + sPolicyNumber , RESOURCE_NOT_FOUND );
        }

        damageBean.setPolicyId( lPolicyId );

        return addDamage( damageBean );
    }

    /**
     * Update damage bean
     * @param damageBean the damage bean
     * @return the damage
     */
    public static DamageBean updateDamageBean( DamageBean damageBean )
        throws Throwable{

        /** Uer has permissions in agenda  */
        Optional
            .ofNullable( damageBean )
            .map(DamageBean::getAgendaId)
            .filter( GrantsDao::hasAuthorizationOnAgendaItem)
            .orElseThrow(() -> new RestServiceException("User has no authorization on update damage ", USER_NO_AUTHORIZED ) );

        ThreadLocalManager.getRecursiveTransaction().merge(damageBean);

        AgendaDao.setUpdateTime( damageBean.getAgendaId() );

        ServiceLogger.info( DamageDao.class , UPDATE , damageBean );

        return damageBean;
    }


    /**
     * Remove damage bean
     * @param lDamageId the damages to be removed
     * @return the damage removed
     */
    public static DamageBean removeDamageBean( Long lDamageId )
        throws Throwable{

        RecursiveTransaction transaction = ThreadLocalManager.getRecursiveTransaction();
        DamageBean damage = Optional
            .ofNullable(lDamageId)
            .map(id -> transaction.find(DamageBean.class, id))
            .orElse(null);

        if (damage == null) {
            throw new RestServiceException("Damage " + lDamageId + " doesn't exist", RESOURCE_NOT_FOUND);
        }

        /** Uer has permissions in agenda  */
        Optional
            .ofNullable(damage)
            .map(DamageBean::getAgendaId)
            .filter(GrantsDao::hasAuthorizationOnAgendaItem)
            .orElseThrow(() ->
                new RestServiceException("User has no authorization on remove damage " + lDamageId, USER_NO_AUTHORIZED));

        transaction.remove(damage);

        AgendaDao.setUpdateTime( damage.getAgendaId() );

        ServiceLogger.info(DamageDao.class, REMOVE, damage);

        return damage;
    }

    /**
     * remove all damages
     * @param lAgendaId the car accident id to remove all damages
     */
    public static void removeDamages( Long lAgendaId )
        throws Throwable {

        List<CarDamageBean> damages = getDamages(lAgendaId);

        if (damages == null || damages.isEmpty()) {
            return;
        }

        RecursiveTransaction transaction = ThreadLocalManager.getRecursiveTransaction();
        transaction.begin();

        try {

            for (CarDamageBean damage : damages) {
                removeDamageBean(damage.getId());
            }

            transaction.commit();
        }
        catch (RestServiceException e ) {
            transaction.rollback();
            throw e;
        }
    }
}
