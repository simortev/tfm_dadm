import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class Main extends HttpServlet {

    private String message;

    private static final String PERSISTENCE_UNIT_NAME = "car_accident_manager";
    private static EntityManagerFactory factory;

    public void init() throws ServletException {
        // Do required initialization
        message = "Hello World";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Set response content type
        response.setContentType("text/html");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        out.println("<h1>" + message + getAgenda() + "</h1>");
    }


    public String getAgenda(){

        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager em = factory.createEntityManager();
        // Read the existing entries and write to console
        Query q = em.createQuery("SELECT event FROM AgendaBean as event");
        List<AgendaBean> events = q.getResultList();

        StringBuilder builder = new StringBuilder();

        for (AgendaBean event : events) {
            System.out.println(  event.getShortDescription() ) ;

            builder.append( event ).append("n");
        }

        em.close();

        return builder.toString();
    }

}