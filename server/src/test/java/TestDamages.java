import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import es.uoc.car.accident.manager.server.jpa.beans.DamageBean;
import es.uoc.car.accident.manager.server.jpa.beans.extended.CarDamageBean;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.DamagesUtils;
import utils.LoginUtils;
import utils.AgendaUtils;
import utils.TestUtils;
import utils.connection.RestConnection;
import utils.properties.Properties;

import static constants.TestConstants.*;

public class TestDamages {

    /** The agenda bean added */
    private static AgendaBean m_carAccident;

    /** Add damage bean */
    private static DamageBean m_damageBean;

    /**
     * Login user , and mantain cookie
     */
    @BeforeClass
    public static void login() {
        RestConnection.setUrl(Properties.getProperty(URL_PROPERTY_LABEL) );
        LoginUtils.login();

        m_carAccident = AgendaUtils.addCarAccident();
    }

    /**
     * Test car accident, basically, add a agendabean, update it, get it and finally remove it
     *
     * We make it in only one test function because we need the functions be executed in order
     */
    @Test
    public void testCarAccident() {
        testAddDamages();
        testGetDamagesByCarAccident();

        testAddDamagesByPolicyNumber();
        testGetDamagesByCarAccident();

        testUpdateDamages();
        testGetDamagesByCarAccident();

        testRemoveDamages();
    }

    /**
     * Test add car accident
     */
    private void testAddDamages () {
        m_damageBean = DamagesUtils.addDamges( m_carAccident.getId() );
        TestUtils.verifyBean(m_damageBean, "add");

    }

    /**
     * Test add car accident
     */
    private void testAddDamagesByPolicyNumber () {
        m_damageBean = DamagesUtils.addDamagesByPolicyNumber( m_carAccident.getId() );
        TestUtils.verifyBean(m_damageBean, "add");

    }

    /**
     * Test get danage beans by car accident
     */
    private void testGetDamagesByCarAccident() {

        String sUrl = String.format( URL_DAMAGES_OF_CAR_ACCIDENT , m_carAccident.getId() );

        CarDamageBean damageBean =
            TestUtils.getBeanByScanId( sUrl, CarDamageBean.class, m_damageBean.getId());

        TestUtils.verifyBean( damageBean, "get damage bean");

        Assert.assertTrue("There was an error get damages by car accident: damages doesn't match",
            m_damageBean.toString().equalsIgnoreCase( damageBean.getDamages().toString()) );
    }

    private void testUpdateDamages() {
        m_damageBean.setDamages( Properties.getProperty(DAMAGES_DESCRIPTION2_LABEL));

        DamageBean damageBean = TestUtils.updateBean( URL_DAMAGES, m_damageBean );
        TestUtils.verifyBean( damageBean, "update damage bean");

        Assert.assertTrue("There was an error update damage bean, damages doesn't match",
            damageBean.toString().equalsIgnoreCase( m_damageBean.toString()) );

        m_damageBean = damageBean;
    }

    private void testRemoveDamages() {
        DamageBean damageBean = TestUtils.removeBean( URL_DAMAGES, DamageBean.class , m_damageBean.getId() );
        TestUtils.verifyBean( damageBean, "remove damage bean");

        Assert.assertTrue("There was an error update damage bean, damages doesn't match",
            damageBean.toString().equalsIgnoreCase( m_damageBean.toString()) );

        String sUrl = String.format( URL_DAMAGES_OF_CAR_ACCIDENT , m_carAccident.getId() );

         CarDamageBean carDamageBean =
            TestUtils.getBeanByScanId( sUrl, CarDamageBean.class, m_damageBean.getId());
         Assert.assertNull( "Damage was no removed" , carDamageBean );
    }

    /**
     * Logout user, invalidate session
     */
    @AfterClass
    public static void logout() {
        TestUtils.removeBean( URL_AGENDA_EVENT , AgendaBean.class , m_carAccident.getId() );
        LoginUtils.logout();
    }

}
