import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import es.uoc.car.accident.manager.server.jpa.beans.extended.CarAccidentBean;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.AgendaUtils;
import utils.LoginUtils;
import utils.TestUtils;
import utils.connection.RestConnection;
import utils.properties.Properties;

import static constants.TestConstants.*;

public class TestCarAccident {

    /** The agenda bean added */
    private static AgendaBean m_carAccident;

    /**
     * Login user , and mantain cookie
     */
    @BeforeClass
    public static void login() {
        RestConnection.setUrl(Properties.getProperty(URL_PROPERTY_LABEL) );
        LoginUtils.login();
    }

    /**
     * Test car accident, basically, add a agendabean, update it, get it and finally remove it
     *
     * We make it in only one test function because we need the functions be executed in order
     */
    @Test
    public void testCarAccident() {
        testAddCarAccident();
        testUpdateCarAccident();
        testGetCarAccidents();
        testRemoveCarAccident();
    }

    /**
     * Test add car accident
     */
    private void testAddCarAccident () {
        m_carAccident = AgendaUtils.addCarAccident();

        TestUtils.verifyBean(m_carAccident, "add");

        AgendaBean carAccident = TestUtils.getBeanById(URL_AGENDA_EVENT, AgendaBean.class, m_carAccident.getId());

        TestUtils.verifyBean(carAccident, "get added bean");

        Assert.assertTrue("There was an error get car accident: agendas doesn't match",
            m_carAccident.toString().equalsIgnoreCase(carAccident.toString()) );
    }

    /**
     * Test update car accident
     */
    private void testUpdateCarAccident () {

        m_carAccident.setCountry(Properties.getProperty(AGENDA_EVENT_COUNTRY_LABEL));

        m_carAccident = TestUtils.updateBean(URL_AGENDA_EVENT, m_carAccident);

        TestUtils.verifyBean(m_carAccident, "update");

        // Get agenda bean from RestFul service and verify it

        AgendaBean carAccident = TestUtils.getBeanById(URL_AGENDA_EVENT, AgendaBean.class, m_carAccident.getId());

        TestUtils.verifyBean(carAccident, "get updated bean");

        Assert.assertTrue("There was an error get car accident: agendas doesn't match",
            m_carAccident.toString().equalsIgnoreCase(carAccident.toString()));
    }

    /**
     * Test get car accidents
     */
    private void testGetCarAccidents () {
        CarAccidentBean carAccident =
            TestUtils.getBeanByScanId(URL_AGENDA_CAR_ACCIDENTS, CarAccidentBean.class, m_carAccident.getId());

        TestUtils.verifyBean(carAccident, "get accident");

       Assert.assertTrue("There was an error get car accident: agendas doesn't match",
            m_carAccident.toString().equalsIgnoreCase( carAccident.getCarAccident().toString()));
    }

    /**
     * Test remove car accident
     */
    private void testRemoveCarAccident(){
        AgendaBean carAccident = TestUtils.removeBean( URL_AGENDA_EVENT , AgendaBean.class , m_carAccident.getId()  );
        TestUtils.verifyBean( carAccident , "get removed bean");

        Assert.assertTrue( "There was an error get car accident: agendas doesn't match" ,
                m_carAccident.toString().equalsIgnoreCase( carAccident.toString() )  );

        AgendaUtils.verifyNoAgendaItem( m_carAccident.getId() , "No car accident" );
    }

    /**
     * Logout user, invalidate session
     */
    @AfterClass
    public static void logout() {
        LoginUtils.logout();
    }

}
