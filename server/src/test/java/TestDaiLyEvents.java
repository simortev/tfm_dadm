import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import es.uoc.car.accident.manager.server.jpa.beans.DamageBean;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.AgendaUtils;
import utils.LoginUtils;
import utils.TestUtils;
import utils.connection.RestConnection;
import utils.properties.Properties;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.*;

import static constants.TestConstants.*;

public class TestDaiLyEvents {

    /** The agenda bean added */
    private static AgendaBean m_accident;

    /** All the tasks */
    private static Map<Long,AgendaBean> m_hTasks;

    /**
     * Login user , and mantain cookie
     */
    @BeforeClass
    public static void login() {

        RestConnection.setUrl(Properties.getProperty(URL_PROPERTY_LABEL) );
       LoginUtils.login();

       // Create agenda event
       m_accident = AgendaUtils.addCarAccident(  );

       m_hTasks =
            AgendaUtils.createChildren( TASKS_NUMBER_BY_CAR_ACCIDENT , m_accident.getId(), AgendaUtils::addReparingCarTask);

    }

    @Test
    public void testEvents() {

        Date date = new Date();

        // Create event for each task
        Map<Long,AgendaBean> hEvents= new HashMap<>();
        Optional
            .ofNullable(m_hTasks)
            .map(Map::keySet)
            .orElseGet(HashSet::new)
            .stream()
            .sequential()
            .map( taskId ->  AgendaUtils.createChildren(EVENTS_NUMBER_BY_TASK,taskId, AgendaUtils::addEvent ) )
            .forEach( hEvents::putAll );

        // Verify all daily events
        verifyDaiLyEvent( hEvents );


        // Verify all monthly events
        verifyMonthlyEvent( hEvents );

        // Verify all the events are for update
        verifyLastEvents( date , hEvents);


        verifyNoLastEvents();

        verifyNoEventsTomorrow();

        // Remove the car accident
        AgendaBean carAccident = TestUtils.removeBean( URL_AGENDA_EVENT , AgendaBean.class , m_accident.getId() );
        TestUtils.verifyBean( carAccident , "Error removing accident");

        // Verify the tasks has been removed
        Optional
            .ofNullable(m_hTasks)
            .map(Map::keySet)
            .orElseGet(HashSet::new)
            .stream()
            .sequential()
            .forEach(id -> AgendaUtils.verifyNoAgendaItem( id , "Task " + id + " exists"));

        // Verify that the car accident has been removed
        AgendaUtils.verifyNoAgendaItem( m_accident.getId() , "Car accident exists" );
    }

    /**
     * Verify the events for each task created in database
     * @param hEvents all the events of the task
     */
    private void verifyDaiLyEvent( Map<Long,AgendaBean> hEvents ) {

        // Get tasks from database
        Map<Long,AgendaBean> hEventsFromDb = AgendaUtils.getDaiLyEvents( );

        Assert.assertTrue( "There was an error in daily events" ,
            AgendaUtils.areEquals( hEvents , hEventsFromDb ) );

    }


    /**
     * Verify the events for each task created in database
     * @param hEvents all the events of the task
     */
    private void verifyMonthlyEvent(Map<Long,AgendaBean> hEvents ) {

        // Get tasks from database
        Map<Long,AgendaBean> hEventsFromDb = AgendaUtils.getMonthLyEvetns( );

        Assert.assertTrue( "There was an error in daily events" ,
            AgendaUtils.areEquals( hEvents , hEventsFromDb ) );

    }

    /**
     *
     * @param hEvents
     */
    private void verifyLastEvents( Date date , Map<Long,AgendaBean> hEvents ) {

        // Get tasks from database
        Map<Long,AgendaBean> hEventsFromDb = AgendaUtils.getLastEvents( date );

        Assert.assertTrue( "There was an error in daily events" ,
            AgendaUtils.areEquals( hEvents , hEventsFromDb ) );

    }

    /**
     * Verify there is no last events
     */
    private void verifyNoEventsTomorrow( ) {
        // Get tasks from database

        LocalDate tomorrow = LocalDate.now().plusDays(1);
        Date minDate = Date.from( LocalDateTime.of(tomorrow,LocalTime.MIDNIGHT)
            .atZone( ZoneId.systemDefault()).toInstant());

        Map<Long,AgendaBean> hEventsFromDb = AgendaUtils.getLastEvents( minDate );

        Assert.assertTrue( "There was an error in daily events" , hEventsFromDb.isEmpty() );
    }

    /**
     * Verify there is no last events
     */
    private void verifyNoLastEvents( ) {
        // Get tasks from database

        Map<Long,AgendaBean> hEventsFromDb = AgendaUtils.getLastEvents( new Date() );

        Assert.assertTrue( "There was an error in daily events" , hEventsFromDb.isEmpty() );
    }

    /**
     * Logout user, invalidate session
     */
    @AfterClass
    public static void logout() {
        LoginUtils.logout();
    }
}
