package utils;

import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import es.uoc.car.accident.manager.server.jpa.beans.UserBean.Role;
import org.junit.Assert;
import utils.connection.RestConnection;
import utils.connection.RestResponse;
import utils.properties.Properties;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static constants.TestConstants.*;
import static es.uoc.car.accident.manager.server.jpa.beans.AgendaBean.AgendaItemSubType.CAR_REPAIRING_TASK;
import static es.uoc.car.accident.manager.server.jpa.beans.AgendaBean.AgendaItemSubType.MEDICAL_EVENT;
import static es.uoc.car.accident.manager.server.jpa.beans.AgendaBean.AgendaItemType.*;

public class AgendaUtils {
    /**
     * Add car accident
     * @return the car accident
     */
    public static AgendaBean addCarAccident() {
        // Create agenda event
        AgendaBean agendaBean = new AgendaBean();
        agendaBean.setCity( Properties.getProperty(AGENDA_EVENT_CITY_LABEL) );
        agendaBean.setShortDescription( Properties.getProperty( AGENDA_EVENT_SHORT_DESCRIPTION_LABEL) );
        agendaBean.setType( CAR_ACCIDENT.ordinal() );
        agendaBean.setPolicyId( Long.parseLong( Properties.getProperty( AGENDA_POLICY_ID_LABEL)));

        /*****************************  START VERIFY EVENT CREATION ************************/
        return TestUtils.addBean( URL_AGENDA_EVENT, agendaBean );
    }

    /**
     * Add task of an accident
     * @param lAccidentId the accident id
     * @return the task added with the id
     */
    public static AgendaBean addReparingCarTask(Long lAccidentId ) {
        AgendaBean agendaBean = new AgendaBean();
        agendaBean.setCity( Properties.getProperty(AGENDA_EVENT_CITY_LABEL) );
        agendaBean.setShortDescription( Properties.getProperty( AGENDA_EVENT_SHORT_DESCRIPTION_LABEL) );
        agendaBean.setParentId( lAccidentId );
        agendaBean.setType( TASK.ordinal() );
        agendaBean.setSubtype(CAR_REPAIRING_TASK.id() );

        return TestUtils.addBean( URL_AGENDA_EVENT , agendaBean );
    }

    /**
     * Add event of a task
     * @param lTaskId the task id
     * @return the event added with the id
     */
    public static AgendaBean addEvent( Long lTaskId ) {
        AgendaBean agendaBean = new AgendaBean();
        agendaBean.setCity( Properties.getProperty(AGENDA_EVENT_CITY_LABEL) );
        agendaBean.setShortDescription( Properties.getProperty( AGENDA_EVENT_SHORT_DESCRIPTION_LABEL) );
        agendaBean.setParentId( lTaskId );
        agendaBean.setType( EVENT.ordinal() );
        agendaBean.setSubtype( MEDICAL_EVENT.id() );

        return TestUtils.addBean( URL_AGENDA_EVENT , agendaBean );
    }

    /**
     * Create several children agenda item
     * @param number the number of children
     * @param lParentId the parent id agenda item
     * @param constructor the function used for creating the agenda item
     * @return The map with all the events of the task by event id
     */
    public static Map<Long,AgendaBean> createChildren(
        Integer number, Long lParentId , Function<Long,AgendaBean> constructor ) {
        return LongStream
            .range( 0 , number )
            .boxed()
            .map( id ->  constructor.apply( lParentId ) )
            .collect( Collectors.toMap( AgendaBean::getId ,  Function.identity() ) );
    }

    /**
     * Verify no bean
     * @param lId the bean id
     * @param operation
     */
    public static void verifyNoAgendaItem( Long lId, String operation ) {
        AgendaBean agendaBean = TestUtils.getBeanById( URL_AGENDA_EVENT, AgendaBean.class , lId );
        Assert.assertNull( operation , agendaBean );
    }

    /**
     * Get children
     * @param lParentId the parent id
     * @return the map of agenda bean items
     */
    public static Map<Long,AgendaBean> getChildren( Long lParentId ) {
        return Optional
            .ofNullable(  RestConnection.getRestList(
                String.format(URL_AGENDA_CHILDREN_EVENT , lParentId )
                , new HashMap<>() , AgendaBean.class  ) )
            .filter( RestResponse::succeed )
            .map( RestResponse::getBean )
            .orElseGet( ArrayList::new )
            .stream()
            .sequential()
            .collect( Collectors.toMap( AgendaBean::getId , Function.identity() ) );
    }

    /**
     * Get children
     * @return the map of agenda bean items
     */
    public static Map<Long,AgendaBean> getDaiLyEvents(  ) {
        return Optional
            .ofNullable(  RestConnection.getRestList(URL_AGENDA_DAILY,  new HashMap<>() , AgendaBean.class  ) )
            .filter( RestResponse::succeed )
            .map( RestResponse::getBean )
            .orElseGet( ArrayList::new )
            .stream()
            .sequential()
            .collect( Collectors.toMap( AgendaBean::getId , Function.identity() ) );
    }

    /**
     * Get children
     * @return the map of agenda bean items
     */
    public static Map<Long,AgendaBean> getMonthLyEvetns(  ) {
        return Optional
            .ofNullable(  RestConnection.getRestList(URL_AGENDA_MONTHLY,  new HashMap<>() , AgendaBean.class  ) )
            .filter( RestResponse::succeed )
            .map( RestResponse::getBean )
            .orElseGet( ArrayList::new )
            .stream()
            .sequential()
            .collect( Collectors.toMap( AgendaBean::getId , Function.identity() ) );
    }

    /**
     * Get children
     * @return the map of agenda bean items
     */
    public static Map<Long,AgendaBean> getLastEvents( Date date ) {
        return Optional
            .ofNullable(  RestConnection.getRestList(String.format(URL_AGENDA_LAST_EVENTS, date.getTime() )
                ,  new HashMap<>() , AgendaBean.class  ) )
            .filter( RestResponse::succeed )
            .map( RestResponse::getBean )
            .orElseGet( ArrayList::new )
            .stream()
            .sequential()
            .collect( Collectors.toMap( AgendaBean::getId , Function.identity() ) );
    }


    /**
     * Compare if both maps are equsls
     * @param hAgendaItems1 the first map
     * @param hAgendaItems2 the second map
     * @return true or false if both maps are equals
     */
    public static boolean areEquals( Map<Long,AgendaBean> hAgendaItems1 ,Map<Long,AgendaBean> hAgendaItems2 ) {

        if( hAgendaItems1 == null || hAgendaItems2 == null || hAgendaItems1.size() != hAgendaItems2.size() )  {
            return false;
        }

        return Optional
            .ofNullable(hAgendaItems1)
            .map(Map::values)
            .orElseGet(HashSet::new)
            .stream()
            .sequential()
            .allMatch( agendaBean -> agendaBean.areEquals( hAgendaItems2.get( agendaBean.getId() ) ) );
    }

    /**
     * Verify user has authorization
     * @param lAgendaId to verify if user has or not grants to get it
     * @param role the user role
     */
    public static void verifyHasAuthorization( Long lAgendaId , Role role ) {
        /** User create car accident */
        LoginUtils.activeUser(role);

        AgendaBean agendaBeanFromDb = TestUtils.getBeanById(URL_AGENDA_EVENT, AgendaBean.class, lAgendaId);
        TestUtils.verifyBean(agendaBeanFromDb, role + " get agenda bean " + lAgendaId );
    }

    /**
     * Verify user has authorization
     * @param agendaBean to verify if user has or not grants to get it
     * @param role the user role
     */
    public static void verifyHasAuthorization( AgendaBean agendaBean , Role role ) {
        LoginUtils.activeUser(role);
        AgendaBean agendaBeanFromDb = TestUtils.getBeanById(URL_AGENDA_EVENT, AgendaBean.class, agendaBean.getId() );
        TestUtils.verifyBean(agendaBeanFromDb, role + " get agenda bean " + agendaBean.getId() );

        Assert.assertTrue(role + ", there was an error get car accident: agendas doesn't match",
            agendaBean.toString().equalsIgnoreCase(agendaBeanFromDb.toString()) );
    }

    /**
     * Verify no authorization to get agenda id by user with role
     * @param lAgendaId the agenda id
     * @param role the role
     * @param role the role
     */
    public static void verifyNoAuthorization( Long lAgendaId , Role role ) {
        /** User create car accident */
        LoginUtils.activeUser(role);
        AgendaUtils
            .verifyNoAgendaItem( lAgendaId , role + " get agenda " + lAgendaId );
    }

    /**
     * Verify the user logged with this role doesn't have grants to access this agenda bean
     * @param agendaBean the agenda bean
     * @param role the role
     */
    public static void verifyNoAuthorization( AgendaBean agendaBean , Role role ) {
        /** User create car accident */
        LoginUtils.activeUser(role);
        AgendaUtils
            .verifyNoAgendaItem( agendaBean.getId() , role + " get agenda " + agendaBean.getId());
    }
}
