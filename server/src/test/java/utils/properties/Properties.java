package utils.properties;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Optional;

import static constants.TestConstants.PROPERTIES_FILE;

/**
 * Class with all the properties used in tests
 */
public class Properties {


    /** Properties from properties file*/
    public static java.util.Properties m_properties;

    /**
     * Load properties
     */
    static
    {
        m_properties = new java.util.Properties();

        try {
            m_properties.load( new InputStreamReader(
                Properties.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE),
                Charset.forName( "UTF-8" ) ));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructor , this calls never can be instantiated
     */
    private Properties() {

    }

    /**
     * Get property value
     * @param sPropertyName the property name
     * @return tge property
     */
    public static String getProperty( String sPropertyName ) {
        return Optional
            .ofNullable( sPropertyName )
            .map( m_properties::getProperty )
            .orElse( null );
    }
}
