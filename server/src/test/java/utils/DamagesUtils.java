package utils;

import es.uoc.car.accident.manager.server.jpa.beans.DamageBean;
import utils.properties.Properties;

import static constants.TestConstants.*;

/**
 * Class used to create damage bean
 */
public class DamagesUtils {

    /**
     * Create a damage bean with this car accident id
     * @param lCarAccident the car accident id
     * @return the damage bean
     */
    public static DamageBean addDamges(Long lCarAccident ) {

        DamageBean damageBean = new DamageBean();

        damageBean.setPolicyId(Long.parseLong(Properties.getProperty(AGENDA_POLICY_ID_LABEL)));
        damageBean.setAgendaId( lCarAccident );
        damageBean.setDamages( Properties.getProperty(DAMAGES_DESCRIPTION1_LABEL));

        return TestUtils.addBean( URL_DAMAGES , damageBean );
    }

    /**
     * Add damages by policy number
     * @param lCarAccident the car accident
     * @return the damage bean
     */
    public static DamageBean addDamagesByPolicyNumber(Long lCarAccident  ) {

        String sPolicyNumber = Properties.getProperty( DAMAGES_POLICY_NUMBER_LABEL );

        DamageBean damageBean = new DamageBean();

        damageBean.setAgendaId( lCarAccident );
        damageBean.setDamages( Properties.getProperty(DAMAGES_DESCRIPTION1_LABEL));

        return TestUtils.addBean( String.format( URL_DAMAGES_BY_POLICY, sPolicyNumber) , damageBean );
    }
}
