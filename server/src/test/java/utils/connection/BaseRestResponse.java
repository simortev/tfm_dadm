package utils.connection;
    /**
    * This the response from a rest request
    */

public class BaseRestResponse {

    /** The status when the request was ok */
    public final static int STATUS_OK = 200;

    /** The status when the request was an error */
    public final static int STATUS_SERVER_ERROR = 500;

    /** The status error*/
    public final static int STATUS_ERROR = 600;

    /** A string with error description if there was an error connection or null if not */
    protected String mError;

    /** The status code */
    protected int mStatus;

    /**
     * Constructor
     * @param error the error description, if can be null
     * @param status the status code
     */
    public BaseRestResponse(String error , int status) {
        this.mError = error;
        this.mStatus = status;
    }

    /**
     * Constructor
     * @param error  the error description, if can be null
     */
    public BaseRestResponse(String error) {
        this.mError = error;
        this.mStatus = STATUS_ERROR;
    }

    public BaseRestResponse(int status) {
        this.mError = null;
        this.mStatus = status;
    }

    public String getError() {
        return mError;
    }

    public void setError(String mError) {
        this.mError = mError;
    }

    public int getStatus() {
        return mStatus;
    }

    public void setStatus(int status) {
        this.mStatus = status;
    }

    /**
     * If connection failed
     * @return true if the connection faild or false in other case
     */
    public boolean fail() {
        return mStatus != STATUS_OK ;
    }

    /**
     * If connection was ok or not
     * @return true if connection was or or false in other case
     */
    public boolean succeed() {
        return mStatus == STATUS_OK;
    }


}
