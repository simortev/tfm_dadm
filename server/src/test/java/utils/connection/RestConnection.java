package utils.connection;


import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.oxm.json.JsonStructureSource;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonReader;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.*;
import java.util.*;
import java.util.function.Function;

import static es.uoc.car.accident.manager.server.constants.CarAccidentServiceConstants.APPLICATION_JSON;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.eclipse.persistence.jaxb.JAXBContextFactory.createContext;
import static utils.connection.BaseRestResponse.STATUS_ERROR;
import static utils.connection.BaseRestResponse.STATUS_OK;


/**
 * This will be a singleton to mantain a Rest connection with the server
 * It will be a singleton that can be accesed by all the activities in order to
 *
 */

public class RestConnection {

    /** The url of the rest service*/
    private static String mUrl;

    /** set the url of the rest connection */
    public static void setUrl( String url )  {
        mUrl = url;
    }

    /**
     * Set url
     * @param manager the cookie manager, is used for have several sessions
     */
    public static void setSession( CookieManager manager )  {
        CookieHandler.setDefault( manager );
    }

    /**
     * This function is used to get a Rest object
     * @param relativeUrl the relative url to the application url
     * @param params the parameters of the url
     * @param <T> the type or response
     * @return the response
     */
    public static <T> utils.connection.RestResponse<T> getRestObject(String relativeUrl, HashMap<String,String> params , Class<T> cl ) {
       utils.connection.RestResponse<String> jsonStringResponse = getJsonStringFromUrl( relativeUrl , params );

        if( jsonStringResponse.getStatus() != STATUS_OK ) {
            return new utils.connection.RestResponse( jsonStringResponse.getError() , jsonStringResponse.getStatus() );
        }

        List<String> json = jsonStringResponse.getBean();
        InputStream stream = new ByteArrayInputStream( json.get(0).getBytes( UTF_8) );
        StreamSource source = new StreamSource(stream);

        return getRestObject( cl ,source , false );
    }

    /**
     * This function is used to get a Rest object as a list
     * @param relativeUrl the relative url to the application url
     * @param params the parameters of the url
     * @param <T> the type or response
     * @return the response
     */
    public static <T> utils.connection.RestResponse<T> getRestList(String relativeUrl, HashMap<String,String> params , Class<T> cl ) {

        utils.connection.RestResponse<String> jsonStringResponse = getJsonStringFromUrl( relativeUrl , params );

        if( jsonStringResponse.getStatus() != STATUS_OK ) {
            return new utils.connection.RestResponse( jsonStringResponse.getError() , jsonStringResponse.getStatus() );
        }

        List<String> json = jsonStringResponse.getBean();
        InputStream stream = new ByteArrayInputStream( json.get( 0 ).getBytes( UTF_8) );
        JsonReader jsonReader = Json.createReader(stream);
        JsonArray customersArray = jsonReader.readArray();
        JsonStructureSource arraySource = new JsonStructureSource(customersArray);

        return getRestObject( cl ,arraySource , true );
    }

    /**
     *
     * @param cl The class to be unmarshaled
     * @param source the Source of the json
     * @return the response
     */
    protected static <T> utils.connection.RestResponse<T> getRestObject(Class<?> cl , Source source , boolean bList ) {

        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        Map<String, Object> jaxbProperties = new HashMap<>(2);
        jaxbProperties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
        jaxbProperties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, false);

        try {
            JAXBContext jc = JAXBContext.newInstance(new Class[]{cl}, jaxbProperties);
            Unmarshaller unmarshaller = jc.createUnmarshaller();

            if( bList == false ) {
                return new utils.connection.RestResponse(Arrays.asList(unmarshaller.unmarshal(source, cl).getValue()));
            }
            else {
                return new utils.connection.RestResponse( (List) unmarshaller.unmarshal(source, cl).getValue() );
            }

        } catch (JAXBException e) {
            e.printStackTrace();
            return new utils.connection.RestResponse( e.getMessage() , STATUS_ERROR );
        }
    }

    /**
     * Add rest object
     * @param sRelativeUrl the relative url
     * @param object the object to be sent
     * @param cl the object to be sent
     * @param <T> the class object to be received
     * @return the response with HTTP status, error, description or final object
     */
    public static <T> utils.connection.RestResponse<T> addRestObject(String sRelativeUrl, T object , Class<T> cl){
        return sendRequestAsMethod( "POST", sRelativeUrl , object , cl );
    }

    /**
     * Update rest object
     * @param sRelativeUrl the relative url
     * @param object the object to be sent
     * @param cl the object to be sent
     * @param <T> the class object to be received
     * @return the response with HTTP status, error, description or final object
     */
    public static <T> utils.connection.RestResponse<T> updateRestObject(String sRelativeUrl, T object , Class<T> cl ){
        return sendRequestAsMethod( "PUT", sRelativeUrl , object , cl );
    }

    /**
     * Remove rest object
     * @param sRelativeUrl the relative urld
     * @param cl the object to be sent
     * @param <T> the class object to be received
     * @return the response with HTTP status, error, description or final object
     */

    public static <T> utils.connection.RestResponse<T> removeRestObject(String sRelativeUrl , Class<T> cl ){
        return sendRequestAsMethod( "DELETE", sRelativeUrl , null , cl );
    }


    /**
     * Send request
     * @param sMethod the HTTP method, POST, PUT or DELETE
     * @param sRelativeUrl the relative url
     * @param object the object to be sent
     * @param cl the object to be sent
     * @param <S> the class object to be send
     * @param <T> the class object to be received
     * @return the response with HTTP status, error, description or final object
     */
    private static <S,T> utils.connection.RestResponse<T> sendRequestAsMethod(
        String sMethod , String sRelativeUrl,  S object , Class<T> cl )
        {
            HttpURLConnection con = null;

            try {
            URL obj = new URL(mUrl + sRelativeUrl);

            Map<String, Object> properties = new HashMap<String, Object>(1);
            properties.put(MarshallerProperties.JSON_WRAPPER_AS_ARRAY_NAME, false);

            con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod(sMethod);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");

            // Send post request
            con.setDoOutput(true);

            if( object != null ) {
                JAXBContext jc = createContext(new Class[]{object.getClass()}, properties);
                Marshaller marshaller = jc.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, APPLICATION_JSON);
                marshaller.marshal(object, con.getOutputStream());

                con.getOutputStream().flush();
            }


            con.getOutputStream().close();

            int statusCode = con.getResponseCode();

            if (statusCode == STATUS_OK) {
                StringBuilder builder = new StringBuilder();
                BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null)
                    builder.append(inputLine);
                in.close();

                InputStream stream = new ByteArrayInputStream( builder.toString().getBytes( UTF_8) );
                StreamSource source = new StreamSource(stream);

                return getRestObject( cl ,source , false );
            } else {
                return new utils.connection.RestResponse(statusCode);
            }
        }
        catch ( Exception e ) {
            return new utils.connection.RestResponse( e.getMessage() , STATUS_ERROR );
        }
        finally {
            if( con != null ){
                con.disconnect();
            }
        }
    }


    /**
     * Get Jons from url
     * @param relativeUrl the relative url to get the json object
     * @param params the params of the HTTP request
     * @return the response as a string
     */
    public static utils.connection.RestResponse<String> getJsonStringFromUrl(String relativeUrl, HashMap<String,String> params )  {
        return getResponseFromUrl( relativeUrl , params , RestConnection::getStringResponse );
    }

    /**
     * Get binary file form url
     * @param relativeUrl the relative url
     * @param params the url params
     * @return the response
     */
    public static utils.connection.RestResponse<byte[]> getBinaryFromUrl(String relativeUrl, HashMap<String,String> params )  {
        return getResponseFromUrl( relativeUrl , params , RestConnection::getBinaryResponse );
    }


    /**
     * Get Jons from url
     * @param relativeUrl the relative url to get the json object
     * @param params the params of the HTTP request
     * @return the response as a string
     */
    public static <T> utils.connection.RestResponse<T> getResponseFromUrl(
        String relativeUrl, HashMap<String,String> params, Function<InputStream, utils.connection.RestResponse<T>> provider )  {
        HttpURLConnection urlConnection = null;

        try {
            URL url = getUrl( relativeUrl , params );

            urlConnection = (HttpURLConnection) url.openConnection();
            int statusCode = urlConnection.getResponseCode();

            InputStream stream = urlConnection.getInputStream();

            if(statusCode == STATUS_OK ) {
                return provider.apply( stream );
            }
            else {
                return new utils.connection.RestResponse( statusCode );
            }
        }
        catch( Exception e ) {
            return new utils.connection.RestResponse( e.getMessage() , STATUS_ERROR );
        }
        finally {
            if( urlConnection != null ){
                urlConnection.disconnect();
            }
        }
    }

    /**
     * Get stream response
     * @param inputStream the input stream from the HTTP response
     * @return a string rest response
     */
    protected static utils.connection.RestResponse<String> getStringResponse(InputStream inputStream ) {

        try {
            StringBuilder builder = new StringBuilder();
            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
            String inputLine;
            while ((inputLine = in.readLine()) != null)
                builder.append(inputLine);
            in.close();

            return new utils.connection.RestResponse<>( Arrays.asList( builder.toString() ));
        }
        catch( Exception e ) {
            return new utils.connection.RestResponse( e.getMessage() , STATUS_ERROR );
        }
    }

    /**
     * Get stream response
     * @param inputStream the input stream from the HTTP response
     * @return a string rest response
     */
    protected static utils.connection.RestResponse<byte[]> getBinaryResponse(InputStream inputStream ) {

        try {

            byte[] bytes = new byte[ inputStream.available()];

            inputStream.read( bytes );

            return new utils.connection.RestResponse<>( Arrays.asList( bytes ) );
        }
        catch( Exception e ) {
            return new utils.connection.RestResponse( e.getMessage() , STATUS_ERROR );
        }
    }

    /**
     * Get url from relative url
     * @param relativeUrl the relative url
     * @param params the url params
     * @return the URL obje t
     */
    private static URL getUrl( String relativeUrl, HashMap<String,String> params  ) {
        String sUrl = mUrl + relativeUrl;
        if( params != null && params.size() != 0 ) {
            sUrl = sUrl + "?";

            int i = params.size();

            for( Map.Entry<String, String> parameter : params.entrySet()) {
                sUrl = sUrl + parameter.getKey() + "=" + parameter.getValue();
                i--;
                if(i != 0) {
                    sUrl = sUrl + "&";
                }
            }
        }

        try {
            return  new URL( sUrl );
        }catch ( MalformedURLException e ) {
            e.printStackTrace();
            return null;
        }
    }

}
