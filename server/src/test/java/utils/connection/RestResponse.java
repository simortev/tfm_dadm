package utils.connection;


import java.util.List;

/**
 * This the response from a rest request
 */

public class RestResponse<T> extends BaseRestResponse  {

    /** Object related with the request*/
    private List<T> mBean;

    /**
     * Constructor
     * @param bean the bean get by a Rest resquest
     * @param error the error description, if can be null
     * @param status the status code
     */
    public RestResponse(List<T> bean, String error , int status) {
        super( error, status );
        this.mBean = bean;
    }

    /**
     * Constructor
     * @param bean he bean get by a Rest resquest
     */
    public RestResponse(List<T> bean ) {
        super( null , STATUS_OK );
        this.mBean = bean;
    }

    /**
     * Constructor
     * @param error  the error description, if can be null
     */
    public RestResponse(String error, int status ) {
        super( error, STATUS_ERROR );
        this.mBean = null;
    }

    public RestResponse(int status) {
        super( null , status );
        this.mBean = null;
    }

    public List<T> getBean() {
        return mBean;
    }

    public void setBean(List<T> mBean) {
        this.mBean = mBean;
    }


}

