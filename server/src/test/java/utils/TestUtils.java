package utils;

import es.uoc.car.accident.manager.server.jpa.beans.interfaces.IObjectBean;
import org.junit.Assert;
import utils.connection.RestConnection;
import utils.connection.RestResponse;

import java.util.*;

import static constants.TestConstants.URL_SEPARATOR;

public class TestUtils {
    /**
     *
     * T bean
     * @param bean the bean to be added
     * @param <T> the template of the bean
     * @return the bean with the id in database
     */
    public static<T> T addBean( String sUrl ,T bean )
    {
        return Optional
            .of( RestConnection.addRestObject( sUrl , bean , ( Class<T>)  bean.getClass() ) )
            .map( TestUtils::getFirstBean )
            .orElse( null );
    }

    /**
     *
     * T bean
     * @param bean the bean to be added
     * @param <T> the template of the bean
     * @return the bean with the id in database
     */
    public static<T> T updateBean( String sUrl ,T bean )
    {
        return Optional
            .of( RestConnection.updateRestObject( sUrl , bean , ( Class<T>) (  bean).getClass() ) )
            .map( TestUtils::getFirstBean )
            .orElse( null );
    }

    /**
     * Get bean by id
     * @param sUrl the url to get the bean
     * @param sId the id of the bean to get
     * @param <T> the template
     * @return the bean requested
     */
    public static<T extends IObjectBean> T getBeanById(String sUrl, Class<T> cl, Long sId )
    {
        return Optional
            .of( RestConnection.getRestObject(
                sUrl + URL_SEPARATOR + sId , new HashMap<>(), cl ) )
            .map( TestUtils::getFirstBean )
            .orElse( null );
    }

    /**
     * Remove the bean
     * @param sUrl the url for removing the bean
     * @param cl the class of the bean
     * @param sId the id of the bean
     * @param <T> the template of the bean
     * @return the bean removed
     */
    public static<T extends IObjectBean> T removeBean(String sUrl, Class<T> cl, Long sId )
    {
        return Optional
            .of( RestConnection.removeRestObject(sUrl + URL_SEPARATOR + sId , cl ) )
            .map( TestUtils::getFirstBean )
            .orElse( null );
    }


    /**
     * Get the bean of a list
     * @param restResponse the response o the rest service
     * @param <T> the type of the bean
     * @return the bean
     */
    private static<T> T getFirstBean( RestResponse<T> restResponse )
    {
        return Optional
            .ofNullable( restResponse )
            .filter( RestResponse::succeed )
            .map( RestResponse::getBean )
            .map( List::iterator )
            .filter( Iterator::hasNext )
            .map( Iterator::next )
            .orElse( null );
    }

    /**
     * Get bean when the url return a list and it is necessary to scan all the values to get the proper
     * @param sUrl the url
     * @param cl the class of beam
     * @param sId the user id
     * @param <T> the template
     * @return the beam
     */
    public static<T extends IObjectBean> T getBeanByScanId(String sUrl, Class<T> cl, Long sId )
    {
        return Optional
            .of( RestConnection.getRestList( sUrl, new HashMap<>(), cl ) )
            .filter( RestResponse::succeed )
            .map( RestResponse::getBean )
            .orElseGet( ArrayList::new)
            .stream()
            .sequential()
            .filter( bean -> bean.getId().compareTo(sId) == 0)
            .findAny()
            .orElse( null );
    }

    /**
     * Verify bean
     * @param bean the bean to be verified
     * @param operation the operation executed before verification
     * @param <T> the template
     */
    public static<T extends IObjectBean> void verifyBean(T bean , String operation )
    {
        Assert.assertNotNull( "There was an error " + operation + ", bean is null" , bean  );

        Assert.assertNotNull( "There was an error " + operation +  ", bean id is null" , bean.getId() );
    }

}

