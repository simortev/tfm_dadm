package utils;

import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import utils.properties.Properties;

import java.util.Date;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static constants.TestConstants.*;
import static es.uoc.car.accident.manager.server.jpa.beans.AgendaBean.AgendaItemSubType.MEDICAL_EVENT;
import static es.uoc.car.accident.manager.server.jpa.beans.AgendaBean.AgendaItemType.CAR_ACCIDENT;
import static es.uoc.car.accident.manager.server.jpa.beans.AgendaBean.AgendaItemType.TASK;

public class CreateDataUtils {

    private static int m_iTaskId = 0;

    private static String sRepeated = IntStream.range(0, 250).mapToObj(i -> "A").collect(Collectors.joining(""));

    /**
     * Add car accident
     * @return the car accident
     */
    public static AgendaBean addCarAccident() {
        // Create agenda event
        AgendaBean agendaBean = new AgendaBean();
        agendaBean.setCity( Properties.getProperty("Lisboa") );
        agendaBean.setCountry("Portugal");
        agendaBean.setLatitude(43.2 );
        agendaBean.setLongitude(3.1);
        agendaBean.setLongDescription( sRepeated );
        agendaBean.setShortDescription( "Frontal accident where a car hit another" );
        agendaBean.setType( CAR_ACCIDENT.ordinal() );
        agendaBean.setPolicyId(1L);
        agendaBean.setStartDate( new Date());


        /*****************************  START VERIFY EVENT CREATION ************************/
        return TestUtils.addBean( URL_AGENDA_EVENT, agendaBean );
    }

    /**
     * Add task of an accident
     * @param lAccidentId the accident id
     * @return the task added with the id
     */
    public static AgendaBean addTask(Long lAccidentId ) {

        AgendaBean agendaBean = new AgendaBean();
        agendaBean.setCity( Properties.getProperty("Lisboa") );
        agendaBean.setCountry("Portugal");
        agendaBean.setParentId(lAccidentId);
        agendaBean.setLatitude(43.2 );
        agendaBean.setLongitude(3.1);
        agendaBean.setLongDescription( sRepeated );
        agendaBean.setShortDescription( String.format(" Task: %d, frontal accident where a car hit another" , m_iTaskId++) );
        agendaBean.setType( TASK.ordinal() );
        agendaBean.setSubtype(AgendaBean.AgendaItemSubType.values()[ getRandomNumberInRange( 0 , 6 ) ].id() );
        agendaBean.setStartDate( new Date());

        return TestUtils.addBean( URL_AGENDA_EVENT , agendaBean );
    }

    /**
     * Add task of an accident
     * @param lTaskId the accident id
     * @return the task added with the id
     */
    public static AgendaBean addEvent(Long lTaskId ) {

        AgendaBean agendaBean = new AgendaBean();
        agendaBean.setCity( Properties.getProperty("Lisboa") );
        agendaBean.setCountry("Portugal");
        agendaBean.setParentId(lTaskId);
        agendaBean.setLatitude(43.2 );
        agendaBean.setLongitude(3.1);
        agendaBean.setLongDescription( sRepeated);
        agendaBean.setShortDescription( String.format(" Event: %d, frontal accident where a car hit another" , m_iTaskId++) );
        agendaBean.setType( TASK.ordinal() );
        agendaBean.setSubtype( MEDICAL_EVENT.id());
        agendaBean.setStartDate( new Date());

        return TestUtils.addBean( URL_AGENDA_EVENT , agendaBean );
    }

    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
}
