package utils;

import es.uoc.car.accident.manager.server.jpa.beans.UserBean;
import es.uoc.car.accident.manager.server.jpa.beans.UserBean.Role;
import es.uoc.car.accident.manager.server.jpa.beans.extended.CarInfoBean;
import utils.connection.RestConnection;
import utils.connection.RestResponse;
import utils.properties.Properties;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.HashMap;

import static constants.TestConstants.*;
import static es.uoc.car.accident.manager.server.jpa.beans.UserBean.Role.CAR_MECHANIC;
import static es.uoc.car.accident.manager.server.jpa.beans.UserBean.Role.CUSTOMER;
import static es.uoc.car.accident.manager.server.jpa.beans.UserBean.Role.INSURANCE_STAFF;

/**
 * Class with utils used in login and logout process
 */
public class LoginUtils {

    /** Cookie manager with the customer session */
    private static CookieManager m_cookieManagerCustomUser;
    /** Cookie manager with the insurance staff worker session */
    private static CookieManager m_cookieManagerInsuranceStaffUser;
    /** Cookie manager with the professional staff session 1 */
    private static CookieManager m_cookieManagerProfessionalStaffUser1;
    /** Cookie manager with the professional staff session 2 */
    private static CookieManager m_cookieManagerProfessionalStaffUser2;

    /**
     * Class only with static functions
     */
    private LoginUtils() {

    }

    /**
     * Login user
     */
    public static void login() {
        login( Properties.getProperty(INSURANCE_STAFF_USER) ,Properties.getProperty(INSURANCE_STAFF_PASSWORD ));
    }

    /**
     * Login with user and password
     * @param sUser the user login
     * @param sPassword the password login
     * @return the cookie manager with the JSESSIONID
     */
    public static CookieManager login(String sUser , String sPassword ) {

        CookieManager cookieManager = new CookieManager( null, CookiePolicy.ACCEPT_ALL);
        RestConnection.setSession(cookieManager );

        // Create parameter
        HashMap<String,String> parameters = new HashMap<>();
        parameters.put( URL_LOGIN_PARAMETER , sUser );
        parameters.put( URL_PASSWORD_PARAMETER , sPassword );

        RestResponse<CarInfoBean> response = RestConnection.getRestObject( URL_LOGIN , parameters  , CarInfoBean.class );

        // If error equal to null, there was no error , the response was got
        if( !response.succeed()  )  {
            System.out.println( response.getError());
        }

        return cookieManager;
    }

    public static void loginUsers() {
        m_cookieManagerCustomUser = LoginUtils.login(
            Properties.getProperty( CUSTOMER_USER ),
            Properties.getProperty( CUSTOMER_PASSWORD )
        );

        m_cookieManagerInsuranceStaffUser = LoginUtils.login(
            Properties.getProperty( INSURANCE_STAFF_USER ),
            Properties.getProperty( INSURANCE_STAFF_PASSWORD )
        );

        m_cookieManagerProfessionalStaffUser1 = LoginUtils.login(
            Properties.getProperty(PROFESSIONAL_STAFF_USER_1),
            Properties.getProperty(PROFESSIONAL_STAFF_PASSWORD_1)
        );

        m_cookieManagerProfessionalStaffUser2 = LoginUtils.login(
            Properties.getProperty(PROFESSIONAL_STAFF_USER_2),
            Properties.getProperty(PROFESSIONAL_STAFF_PASSWORD_2)
        );
    }

    /**
     * Active user by role
     * @param role the user role
     */
    public static void activeUser( Role role ) {

        if( role == CUSTOMER ) {
            RestConnection.setSession( m_cookieManagerCustomUser);
        }
        else if ( role == INSURANCE_STAFF){
            RestConnection.setSession( m_cookieManagerInsuranceStaffUser);
        }
        else if( role == CAR_MECHANIC ) {
            RestConnection.setSession(m_cookieManagerProfessionalStaffUser1);
        }
        else {
            RestConnection.setSession(m_cookieManagerProfessionalStaffUser2);
        }
    }

    /**
     * Logout user, invalidate session
     */
    public static void logout() {

        // Create parameters
        HashMap<String,String> parameters = new HashMap<>();

        RestResponse<CarInfoBean> response = RestConnection.getRestObject( URL_LOGOUT, parameters , CarInfoBean.class );

        if( !response.succeed()  )  {
            System.out.println( response.getError());
        }
    }

    public static void logoutUsers() {

        RestConnection.setSession( m_cookieManagerCustomUser);
        LoginUtils.logout();


        RestConnection.setSession( m_cookieManagerInsuranceStaffUser);
        LoginUtils.logout();


        RestConnection.setSession(m_cookieManagerProfessionalStaffUser1);
        LoginUtils.logout();

        RestConnection.setSession(m_cookieManagerProfessionalStaffUser2);
        LoginUtils.logout();
    }
}
