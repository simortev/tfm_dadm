package utils.http;

import java.io.IOException;
import java.io.Writer;

public class LogWriter extends Writer {

    private Writer m_writer;

    public LogWriter( Writer writer ) {
        super();
        m_writer = writer;
    }

    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {

        System.out.print( new String(cbuf).substring( off , len - off ));

        m_writer.write( cbuf, off, len);
    }

    @Override
    public void flush() throws IOException {
        m_writer.flush();
    }

    @Override
    public void close() throws IOException {
        m_writer.close();
    }
}
