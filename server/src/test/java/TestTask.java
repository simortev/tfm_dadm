import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.AgendaUtils;
import utils.LoginUtils;
import utils.TestUtils;
import utils.connection.RestConnection;
import utils.properties.Properties;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;

import static constants.TestConstants.TASKS_NUMBER_BY_CAR_ACCIDENT;
import static constants.TestConstants.URL_AGENDA_EVENT;
import static constants.TestConstants.URL_PROPERTY_LABEL;

public class TestTask {

    /** The agenda bean added */
    private static AgendaBean m_accident;

    /**
     * Login user , and cookie
     */
    @BeforeClass
    public static void login() {
        RestConnection.setUrl(Properties.getProperty(URL_PROPERTY_LABEL) );
        // User login
        LoginUtils.login();

        // Create agenda event
        m_accident = AgendaUtils.addCarAccident(  );
    }


    @Test
    public void testTask() {
        // Create the tasks under the car accident
        Map<Long,AgendaBean> hTasks =
            AgendaUtils.createChildren(TASKS_NUMBER_BY_CAR_ACCIDENT, m_accident.getId(), AgendaUtils::addReparingCarTask);
        Assert.assertEquals( "There are no " + TASKS_NUMBER_BY_CAR_ACCIDENT,
            hTasks.size() , TASKS_NUMBER_BY_CAR_ACCIDENT);

        // Get tasks from database
        Map<Long,AgendaBean> hTasksFromDb = AgendaUtils.getChildren( m_accident.getId() );
        Assert.assertEquals( "There are no " + TASKS_NUMBER_BY_CAR_ACCIDENT + " in car accident" ,
            hTasksFromDb.size() , TASKS_NUMBER_BY_CAR_ACCIDENT);

        Assert.assertTrue( "There was an error adding tasks" ,
            AgendaUtils.areEquals( hTasks , hTasksFromDb ) );

        // Remove the car accident
        AgendaBean carAccident = TestUtils.removeBean( URL_AGENDA_EVENT , AgendaBean.class , m_accident.getId() );
        TestUtils.verifyBean( carAccident , "Error removing accident");

        // Verify the tasks has been removed
        Optional
            .of(hTasks)
            .map(Map::keySet)
            .orElseGet(HashSet::new)
            .stream()
            .sequential()
            .forEach(id -> AgendaUtils.verifyNoAgendaItem( id , "Task " + id + " exists"));

        // Verify that the car accident has been removed
        AgendaUtils.verifyNoAgendaItem( m_accident.getId() , "Car accident exists" );
    }

    /**
     * Logout user, invalidate session
     */
    @AfterClass
    public static void logout() {
        LoginUtils.logout();
    }
}
