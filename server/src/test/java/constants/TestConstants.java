package constants;

public class TestConstants {

    /**************************** URL CONSTANTS ***************************/

    /** The url separator */
    public final static String URL_SEPARATOR = "/";

    /** The login url */
    public final static String URL_LOGIN = "/login";

    /** The login parameter of the url */
    public final static String URL_LOGIN_PARAMETER = "login";

    /** The password parameter of the url */
    public final static String URL_PASSWORD_PARAMETER = "password";


    /** The form file parameter */
    public final static String FORM_FILE_PARAMETER = "file";

    /** The form file info parameter */
    public final static String FORM_FILE_INFO_PARAMETER = "info";

    /** The logout url */
    public final static String URL_LOGOUT = "/logout";

    /** The agenda event url */
    public final static String URL_AGENDA_EVENT = "/rest/agenda/element";

    /** Url to get the children of agenda */
    public final static String URL_AGENDA_CHILDREN_EVENT = "/rest/agenda/agenda/%d";

    /** Url to get the daily events */
    public final static String URL_AGENDA_DAILY = "/rest/agenda/element/daily";

    /** Url to get the monthly events */
    public final static String URL_AGENDA_MONTHLY = "/rest/agenda/element/monthly";

    /** Url to get the last evetns */
    public final static String URL_AGENDA_LAST_EVENTS = "/rest/agenda/element/last/%d";

    /** The agenda car accidents url */
    public final static String URL_AGENDA_CAR_ACCIDENTS = "/rest/agenda/accidents";

    /** The document url */
    public final static String URL_DOCUMENT = "/rest/document/element";

    /** The agenda documents url */
    public final static String URL_AGENDA_DOCUMENTS = "/rest/document/agenda/%d";

    /** The damages url */
    public final static String URL_DAMAGES = "/rest/damages/element";

    /** Get damages by car accident*/
    public final static String URL_DAMAGES_OF_CAR_ACCIDENT = "/rest/damages/agenda/%d";

    /** The damages url */
    public final static String URL_DAMAGES_BY_POLICY = "/rest/damages/policy/%s";

    /**************************** PROPERTIES CONSTANTS ***************************/
    /** The properties file */
    public final static String PROPERTIES_FILE = "test.properties";

    /**the url property label */
    public final static String URL_PROPERTY_LABEL = "url";

    /** The bad user property label */
    public final static String BAD_USER_PROPERTY_LABEL = "bad_user";

    /** The bad password property label */
    public final static String BAD_PASSWORD_PROPERTY_LABEL = "bad_password";

    /** Agenda event to get short description*/
    public final static String AGENDA_EVENT_SHORT_DESCRIPTION_LABEL =
        "agenda.car.accident.short_description";

    /** Agenda event label to get country label */
    public final static String AGENDA_EVENT_COUNTRY_LABEL =
        "agenda.car.accident.country";

    /** Agenda event label to get city label*/
    public final static String AGENDA_EVENT_CITY_LABEL =
        "agenda.car.accident.city";

    public final static String AGENDA_POLICY_ID_LABEL =
        "agenda.car.accident.policy.id";

    public final static String DAMAGES_POLICY_NUMBER_LABEL =
        "agenda.car.accident.damages.policy.number";

    /** The damage description 1 label */
    public final static String DAMAGES_DESCRIPTION1_LABEL =
        "agenda.car.accident.damages.description1";

    /** The damage description 2 label */
    public final static String DAMAGES_DESCRIPTION2_LABEL =
        "agenda.car.accident.damages.description2";

    /** The customer user login */
    public final static String CUSTOMER_USER ="customer_user";

    /** The customer user password */
    public final static String CUSTOMER_PASSWORD ="customer_password";

    /** The insurance staff user login */
    public final static String INSURANCE_STAFF_USER ="insurance_staff_user";

    /** The insurance staff user password */
    public final static String INSURANCE_STAFF_PASSWORD ="insurance_staff_password";

    /** The professional staff user login 1*/
    public final static String PROFESSIONAL_STAFF_USER_1 ="professional_staff_user_1";

    /** The professional staff passwrod 1*/
    public final static String PROFESSIONAL_STAFF_PASSWORD_1 ="professional_staff_password_1";

    /** The professional staff user login 2*/
    public final static String PROFESSIONAL_STAFF_USER_2 ="professional_staff_user_2";

    /** The professional staff passwrod 2*/
    public final static String PROFESSIONAL_STAFF_PASSWORD_2 ="professional_staff_password_2";

    /***/
    public final static int TASKS_NUMBER_BY_CAR_ACCIDENT = 4;

    public final static int EVENTS_NUMBER_BY_TASK = 4;

    /**
     * A class with static member, never can be created
     */
    private TestConstants() {

    }


}
