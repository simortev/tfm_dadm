import constants.TestConstants;
import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import es.uoc.car.accident.manager.server.jpa.beans.DamageBean;
import es.uoc.car.accident.manager.server.jpa.beans.DocumentBean;
import es.uoc.car.accident.manager.server.jpa.beans.UserBean;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.AgendaUtils;
import utils.LoginUtils;
import utils.TestUtils;
import utils.connection.RestConnection;
import utils.connection.RestResponse;
import utils.http.MultipartUtility;
import utils.properties.Properties;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static constants.TestConstants.*;
import static es.uoc.car.accident.manager.server.constants.CarAccidentServiceConstants.APPLICATION_JSON;
import static org.eclipse.persistence.jaxb.JAXBContextFactory.createContext;


public class TestDocument {

    /** The agenda bean added */
    private static AgendaBean m_carAccident;


    /**
     * Login user , and mantain cookie
     */
    @BeforeClass
    public static void login() {
        RestConnection.setUrl(Properties.getProperty(URL_PROPERTY_LABEL) );
        LoginUtils.login();

        m_carAccident = AgendaUtils.addCarAccident();
    }

    /**
     * + Add an new agenda event  ( get it and verify the operation was well performed )
     * + Update agenda event ( get it and verify the operation was well performed )
     * + Remove agenda event ( get it and verify no event has been received )
     */
    @Test
    public void testAddDocument() {

        //*************** ADD DOCUMENTS *******************
        String sURL =
            Properties.getProperty( TestConstants.URL_PROPERTY_LABEL) + URL_DOCUMENT;

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(PROPERTIES_FILE).getFile());

       addDocument( sURL, m_carAccident.getId() , PROPERTIES_FILE + "1" , "Short description" , file);

       addDocument( sURL, m_carAccident.getId() , PROPERTIES_FILE + "2","Short description", file);

       addDocument( sURL, m_carAccident.getId() , PROPERTIES_FILE + "3" ,"Short description", file);

        //Get agenda bean from RestFul service and verify it
        RestResponse<DocumentBean> response =
            RestConnection.getRestList( String.format(URL_AGENDA_DOCUMENTS , m_carAccident.getId() ) , new HashMap<>() ,DocumentBean.class );

        Assert.assertTrue( "There was an error get document: getRestObject" , response.succeed() );

        Assert.assertEquals( "There was an error get document of agenda event: getRestObject" ,
            response.getBean().size() , 3 );

        //************** GET DOCUMENTS ******************

        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] original_document = new byte[fileInputStream.available()];
           fileInputStream.read(original_document);
            fileInputStream.close();

            for( DocumentBean documentBean : response.getBean() ) {
                byte[] remote_document = getDocument( documentBean );

                Assert.assertTrue( "Files are not equal" ,
                    Arrays.equals( original_document , remote_document ));
            }
        }
        catch( Exception e ) {
            e.printStackTrace();
        }


        for( DocumentBean documentBean : response.getBean() ) {


           response =
                RestConnection.removeRestObject( URL_DOCUMENT + URL_SEPARATOR + documentBean.getId() , DocumentBean.class );

            Assert.assertTrue( "There was an error get document: getRestObject" , response.succeed()  );
        }

    }

    /**
     * Add document
     * @param sURL the url of the JSON service
     * @param lAgendaId the agenda id to attach the document
     * @param sFileName the file name
     * @param sDescription the file name
     * @param file the file
     */
    private  void addDocument( String sURL, Long lAgendaId, String sFileName, String sDescription,File file ) {

        try {
            MultipartUtility multipartUtility = new MultipartUtility( sURL, StandardCharsets.UTF_8.name());

            DocumentBean documentBean =
                DocumentBean.createDocumentBean( null, lAgendaId , sFileName , sDescription);

            Map<String, Object> properties = new HashMap<>(1);
            properties.put(MarshallerProperties.JSON_WRAPPER_AS_ARRAY_NAME, false);

            JAXBContext jc = createContext(new Class[]{DocumentBean.class}, properties);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, APPLICATION_JSON);
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            marshaller.marshal(documentBean, out );

            multipartUtility.addFilePart( FORM_FILE_PARAMETER, file ,  MediaType.APPLICATION_OCTET_STREAM );
            multipartUtility.addFormField( FORM_FILE_INFO_PARAMETER,  out.toString() , MediaType.TEXT_PLAIN);

            multipartUtility.finish();
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
    }


    /**
     * Get document
     * @param documentBean the document bean
     * @return the byte array of the document
     */
    private byte[] getDocument( DocumentBean documentBean ) {

        // Get document bean from RestFul service and verify it
        RestResponse<byte[]> response =
            RestConnection.getBinaryFromUrl( URL_DOCUMENT + URL_SEPARATOR + documentBean.getId() , new HashMap<>()  );

        Assert.assertTrue( "There was an error get document: getRestObject" , response.succeed()  );

        return response.getBean().get( 0 );
    }

    /**
     * Logout user, invalidate session
     */
    @AfterClass
    public static void logout() {
        TestUtils.removeBean( URL_AGENDA_EVENT , AgendaBean.class , m_carAccident.getId() );
        LoginUtils.logout();
    }

}

