import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.AgendaUtils;
import utils.LoginUtils;
import utils.TestUtils;
import utils.connection.RestConnection;
import utils.properties.Properties;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;

import static constants.TestConstants.*;

public class TestEvents {

    /** The agenda bean added */
    private static AgendaBean m_accident;

    /** All the tasks */
    private static Map<Long,AgendaBean> m_hTasks;

    /**
     * Login user , and mantain cookie
     */
    @BeforeClass
    public static void login() {

        RestConnection.setUrl(Properties.getProperty(URL_PROPERTY_LABEL) );
       LoginUtils.login();

       // Create agenda event
       m_accident = AgendaUtils.addCarAccident(  );

       m_hTasks =
            AgendaUtils.createChildren( TASKS_NUMBER_BY_CAR_ACCIDENT , m_accident.getId(), AgendaUtils::addReparingCarTask);

    }

    @Test
    public void testEvents() {

        // Create event for each task
        Map<Long,Map<Long,AgendaBean>> hEventsByTask = new HashMap<>();
        Optional
            .ofNullable(m_hTasks)
            .map(Map::keySet)
            .orElseGet(HashSet::new)
            .stream()
            .sequential()
            .forEach( taskId ->
                hEventsByTask.put( taskId ,
                    AgendaUtils.createChildren(EVENTS_NUMBER_BY_TASK,taskId, AgendaUtils::addEvent ) ) );

        // Verify all the events
        Optional
            .of(hEventsByTask)
            .map(Map::entrySet)
            .orElseGet(HashSet::new)
            .stream()
            .sequential()
            .forEach( e -> verifyTask( e.getKey() , e.getValue() ) );


        // Remove the car accident
        AgendaBean carAccident = TestUtils.removeBean( URL_AGENDA_EVENT , AgendaBean.class , m_accident.getId() );
        TestUtils.verifyBean( carAccident , "Error removing accident");

        // Verify the tasks has been removed
        Optional
            .ofNullable(m_hTasks)
            .map(Map::keySet)
            .orElseGet(HashSet::new)
            .stream()
            .sequential()
            .forEach(id -> AgendaUtils.verifyNoAgendaItem( id , "Task " + id + " exists"));

        // Verify if the events exist
        Optional
            .of(hEventsByTask)
            .map(Map::values)
            .orElseGet(HashSet::new)
            .stream()
            .sequential()
            .flatMap( map -> map.keySet().stream() )
         //   .map( eventId ->{System.out.println( eventId ); return eventId;} )
            .forEach( eventId -> AgendaUtils.verifyNoAgendaItem( eventId, "Event " + eventId + " exists") );

        // Verify that the car accident has been removed
        AgendaUtils.verifyNoAgendaItem( m_accident.getId() , "Car accident exists" );
    }

    /**
     * Verify the events for each task created in database
     * @param lTaskId the task id
     * @param hEvents all the events of the task
     */
    private void verifyTask( Long lTaskId , Map<Long,AgendaBean> hEvents ) {

        // Get tasks from database
        Map<Long,AgendaBean> hEventsFromDb = AgendaUtils.getChildren( lTaskId );
        Assert.assertEquals( "There are no " + EVENTS_NUMBER_BY_TASK + " in car accident" ,
            hEventsFromDb.size() , EVENTS_NUMBER_BY_TASK);

        Assert.assertTrue( "There was an error adding tasks" ,
            AgendaUtils.areEquals( hEvents , hEventsFromDb ) );

    }


    /**
     * Logout user, invalidate session
     */
    @AfterClass
    public static void logout() {
        LoginUtils.logout();
    }
}
