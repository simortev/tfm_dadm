import es.uoc.car.accident.manager.server.jpa.beans.UserBean;
import es.uoc.car.accident.manager.server.jpa.beans.extended.CarInfoBean;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.connection.RestConnection;
import utils.connection.RestResponse;
import utils.properties.Properties;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.HashMap;

import static constants.TestConstants.*;

/**
 * This class is used to verify login and logout servlets
 *
 * + Verify login ok
 * + Verify login user error
 * + Verify login password error
 * + Verify logout
 *
 */
public class TestLogin  {


    @BeforeClass
    public static void setURL() {
        RestConnection.setUrl(Properties.getProperty( URL_PROPERTY_LABEL ) );
        RestConnection.setSession( new CookieManager( null , CookiePolicy.ACCEPT_ALL ));
    }

    /**
     * Test loing ok
     */
    @Test
    public void testLoginOk() {
        // Create parameters
        HashMap<String,String> parameters = new HashMap<>();
        parameters.put( URL_LOGIN_PARAMETER , Properties.getProperty(INSURANCE_STAFF_USER ) );
        parameters.put( URL_PASSWORD_PARAMETER , Properties.getProperty(INSURANCE_STAFF_PASSWORD ) );

        RestResponse<CarInfoBean> response = RestConnection.getRestObject( URL_LOGIN , parameters , CarInfoBean.class );

        Assert.assertTrue( response.getError() , response.succeed()  );

        logout();
    }

    /**
     * Test login user doesn't exist
     */
    @Test
    public void testLoginUserError() {
        // Create parameters
        HashMap<String,String> parameters = new HashMap<>();
        parameters.put( URL_LOGIN_PARAMETER , Properties.getProperty(BAD_USER_PROPERTY_LABEL ) );
        parameters.put( URL_PASSWORD_PARAMETER , Properties.getProperty(INSURANCE_STAFF_PASSWORD ) );

        RestResponse<CarInfoBean> response = RestConnection.getRestObject( URL_LOGIN, parameters , CarInfoBean.class );

        Assert.assertFalse( response.getError() , response.succeed()  );
    }

    /**
     * Test password it is not correct
     */
    @Test
    public void testPasswordError() {
        // Create parameters
        HashMap<String,String> parameters = new HashMap<>();
        parameters.put( URL_LOGIN_PARAMETER ,  Properties.getProperty(INSURANCE_STAFF_USER )  );
        parameters.put( URL_PASSWORD_PARAMETER , Properties.getProperty(BAD_PASSWORD_PROPERTY_LABEL ) );

        RestResponse<CarInfoBean> response = RestConnection.getRestObject( URL_LOGIN , parameters , CarInfoBean.class );

        Assert.assertFalse( response.getError() , response.succeed()  );
    }

    /**
     * User logout
     */
    public static void logout() {

        // Create parameters
        HashMap<String,String> parameters = new HashMap<>();

        RestResponse<CarInfoBean> response = RestConnection.getRestObject( URL_LOGOUT , parameters , CarInfoBean.class );

        Assert.assertTrue( response.getError() , response.succeed()  );

    }

}
