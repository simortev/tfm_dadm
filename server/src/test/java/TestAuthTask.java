import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import es.uoc.car.accident.manager.server.jpa.beans.UserBean.Role;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.LoginUtils;
import utils.AgendaUtils;
import utils.TestUtils;
import utils.connection.RestConnection;
import utils.properties.Properties;

import static constants.TestConstants.*;
import static es.uoc.car.accident.manager.server.jpa.beans.UserBean.Role.*;
import static utils.AgendaUtils.verifyHasAuthorization;
import static utils.AgendaUtils.verifyNoAuthorization;

public class TestAuthTask {

    /** The agenda bean added */
    private static AgendaBean m_carAccident;

    /**
     * Login user , and  cookie
     */
    @BeforeClass
    public static void login() {
        RestConnection.setUrl(Properties.getProperty(URL_PROPERTY_LABEL) );
        LoginUtils.loginUsers();
    }

    @Test
    public void testTask() {
        LoginUtils.activeUser(CUSTOMER);
        m_carAccident = AgendaUtils.addCarAccident();

        // Customers and professional staff can't add a task
        addTaskFailure( CUSTOMER );
        addTaskFailure( CAR_MECHANIC );

        addTask();

        LoginUtils.activeUser(CUSTOMER);
        AgendaBean agendaBean =
            TestUtils.removeBean( URL_AGENDA_EVENT , AgendaBean.class , m_carAccident.getId() );
        Assert.assertNull( "Customer doesn't have permission to remove bean" , agendaBean);

        LoginUtils.activeUser(CAR_MECHANIC);
         agendaBean =
            TestUtils.removeBean( URL_AGENDA_EVENT , AgendaBean.class , m_carAccident.getId() );
        Assert.assertNull( "Car mechanic doesn't have permission to remove bean" , agendaBean);

        LoginUtils.activeUser(INSURANCE_STAFF);
         agendaBean =
            TestUtils.removeBean( URL_AGENDA_EVENT , AgendaBean.class , m_carAccident.getId() );
        Assert.assertNotNull( "Insurance staff have permission to remove bean" , agendaBean);
    }

    /**
     * Add a tasak by customer or car mechanic , they can't do it, therefore an error is received
     * @param role customer or professional staff
     */
    private void addTaskFailure( Role role ) {

        /// Role create task
        LoginUtils.activeUser(role);

        AgendaBean task = AgendaUtils.addReparingCarTask( m_carAccident.getId());
        Assert.assertNull(role + ", there was an error add task accident with  no grants", task );
    }

    private void addTask() {
        // Role create car accident
        LoginUtils.activeUser(INSURANCE_STAFF);

        AgendaBean task = AgendaUtils.addReparingCarTask( m_carAccident.getId());
        TestUtils.verifyBean( task , "add tasks" );

        // Verify every has grants on tasks but also car driver
        AgendaUtils.verifyHasAuthorization( task , INSURANCE_STAFF );
        AgendaUtils.verifyHasAuthorization( task , CUSTOMER );
        AgendaUtils.verifyHasAuthorization( task , CAR_MECHANIC );
        // No authorization
        AgendaUtils.verifyNoAuthorization(task , CAR_DRIVER);

        // Verify everybody has grants on car accident but also car driver
        AgendaUtils.verifyHasAuthorization( task.getParentId() ,INSURANCE_STAFF );
        AgendaUtils.verifyHasAuthorization( task.getParentId() , CUSTOMER);
        AgendaUtils.verifyHasAuthorization( task.getParentId() , CAR_MECHANIC);

        // No authorization
        AgendaUtils.verifyNoAuthorization(task.getParentId() , CAR_DRIVER);
    }

    /**
     * Logout user, invalidate session
     */
    @AfterClass
    public static void logout() {
        LoginUtils.logoutUsers();
    }

}
