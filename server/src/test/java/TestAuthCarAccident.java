import es.uoc.car.accident.manager.server.jpa.beans.AgendaBean;
import es.uoc.car.accident.manager.server.jpa.beans.UserBean.Role;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.AgendaUtils;
import utils.LoginUtils;
import utils.TestUtils;
import utils.connection.RestConnection;
import utils.properties.Properties;

import java.util.Date;

import static constants.TestConstants.*;
import static es.uoc.car.accident.manager.server.jpa.beans.UserBean.Role.*;
import static utils.AgendaUtils.verifyHasAuthorization;
import static utils.AgendaUtils.verifyNoAuthorization;

public class TestAuthCarAccident {

    /**
     * Login user , and  cookie
     */
    @BeforeClass
    public static void login() {
        RestConnection.setUrl(Properties.getProperty(URL_PROPERTY_LABEL) );
        LoginUtils.loginUsers();
    }

    @Test
    public void testCarAccident() {
        addCarAccidentBy( CUSTOMER );
        addCarAccidentBy( INSURANCE_STAFF );
        addCarAccidentFailure(  );

        updateCarAccidentWhenCreatedBy( CUSTOMER );
        updateCarAccidentWhenCreatedBy( INSURANCE_STAFF );
    }

    /**
     * Add correctly an accident car by a role
     * @param role the role of user to ad car
     */
    private void addCarAccidentBy( Role role ) {

        // Role create car accident
        LoginUtils.activeUser(role);

        AgendaBean carAccident1 = AgendaUtils.addCarAccident();
        TestUtils.verifyBean(carAccident1, "add car accident");

        // Verify customer and insurance staff has grants, the role car mechanic doesn't have permissions
        verifyHasAuthorization( carAccident1 , CUSTOMER );
        verifyHasAuthorization( carAccident1 , INSURANCE_STAFF );
        verifyNoAuthorization( carAccident1 , CAR_MECHANIC );
        verifyNoAuthorization( carAccident1 , CAR_DRIVER );

        // Customer can't remove car accident
        LoginUtils.activeUser(CUSTOMER);
        AgendaBean bean = TestUtils.removeBean( URL_AGENDA_EVENT , AgendaBean.class , carAccident1.getId() );
        Assert.assertNull( "Customer doesn't have permissions to remove the agenda bean" , bean );

        // Insurance staff remove car accident
        LoginUtils.activeUser(INSURANCE_STAFF);
        bean = TestUtils.removeBean( URL_AGENDA_EVENT , AgendaBean.class , carAccident1.getId() );
        Assert.assertNotNull( "Insurance staff doesn't have permissions to remove the agenda bean" , bean );
    }

    /**
     * Add correctly an accident car by a role
     * @param role the role of user to update car info
     */
    private void updateCarAccidentWhenCreatedBy( Role role ) {

        // Role create car accident
        LoginUtils.activeUser(role);

        AgendaBean carAccident1 = AgendaUtils.addCarAccident();
        TestUtils.verifyBean(carAccident1, "add car accident");

        //Verify customer and insurance staff has grants, the role car mechanic doesn't have permissions

        verifyHasAuthorization( carAccident1 , CUSTOMER );
        verifyHasAuthorization( carAccident1 , INSURANCE_STAFF );
        verifyNoAuthorization( carAccident1 , CAR_MECHANIC );
        verifyNoAuthorization( carAccident1 , CAR_DRIVER );

        // update car accident by customer
        LoginUtils.activeUser(CUSTOMER);
        carAccident1.setCountry( Properties.getProperty(AGENDA_EVENT_COUNTRY_LABEL));
        TestUtils.updateBean( URL_AGENDA_EVENT , carAccident1 );

        verifyHasAuthorization( carAccident1 , CUSTOMER );
        verifyHasAuthorization( carAccident1 , INSURANCE_STAFF );
        verifyNoAuthorization( carAccident1 , CAR_MECHANIC );
        verifyNoAuthorization( carAccident1 , CAR_DRIVER );

        // update car accident by insurance staff
        LoginUtils.activeUser(INSURANCE_STAFF);
        carAccident1.setEndDate( new Date() );
        TestUtils.updateBean( URL_AGENDA_EVENT , carAccident1 );

        verifyHasAuthorization( carAccident1 , CUSTOMER );
        verifyHasAuthorization( carAccident1 , INSURANCE_STAFF );
        verifyNoAuthorization( carAccident1 , CAR_MECHANIC );
        verifyNoAuthorization( carAccident1 , CAR_DRIVER );

        // failure update car accident by professional staff
        LoginUtils.activeUser(CAR_MECHANIC);
        carAccident1.setLongDescription( CAR_MECHANIC.name() );
        AgendaBean agendaBean = TestUtils.updateBean( URL_AGENDA_EVENT , carAccident1 );
        Assert.assertNull( " car mechanics there was an error update car accident with  no grants", agendaBean );


        // Car accident hasn't been updated
        carAccident1.setLongDescription( null );
        verifyHasAuthorization( carAccident1 , CUSTOMER );
        verifyHasAuthorization( carAccident1 , INSURANCE_STAFF );

        // Customer can't remove car accident
        LoginUtils.activeUser(CUSTOMER);
        AgendaBean bean = TestUtils.removeBean( URL_AGENDA_EVENT , AgendaBean.class , carAccident1.getId() );
        Assert.assertNull( "Customer doesn't have permissions to remove the agenda bean" , bean );

        // Insurance staff remove car accident
        LoginUtils.activeUser(INSURANCE_STAFF);
        bean = TestUtils.removeBean( URL_AGENDA_EVENT , AgendaBean.class , carAccident1.getId() );
        Assert.assertNotNull( "Insurance staff doesn't have permissions to remove the agenda bean" , bean );
    }


    /**
     * Verify user doesn't have authorization to add a car accident
     */
    private void addCarAccidentFailure(  ) {
        LoginUtils.activeUser(CAR_MECHANIC);
        AgendaBean carAccident1 = AgendaUtils.addCarAccident();
        Assert.assertNull(CAR_MECHANIC + ", there was an error add car accident with  no grants", carAccident1 );
    }

    /**
     * Logout user, invalidate session
     */
    @AfterClass
    public static void logout() {
        LoginUtils.logoutUsers();
    }

}
